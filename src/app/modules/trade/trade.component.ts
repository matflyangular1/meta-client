import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Renderer2, Injector } from '@angular/core';
import { BaseComponent } from '../../share/components/common.component';
import { ModalService, AuthService, SharedataService, ConstantsService, SocketService, WebsocketService } from '../../core/services';
import { Trade, TradeList } from '../../share/models/trade';
import { cHtmlHelper } from '../../share/helper/html.helper';
import { Subscription, timer } from 'rxjs';
import swal from 'sweetalert2';
import { it } from '@angular/core/testing/src/testing_internal';

export let isweekend = false;

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css'],
  providers: [cHtmlHelper] // , SocketService, WebsocketService
})
export class TradeComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('positionRef') positionRef: ElementRef;
  @ViewChild('ordersRef') ordersRef: ElementRef;

  public filterBtn: string = 'Today';
  public tradePositionList: TradeList[] = [];
  public tradeOrdersList: TradeList[] = [];
  public dapthArr: any = [];
  public orderOptionList: any;
  public sellOptions: any;
  public lotArr: any;
  public liveData: any;
  public orderDetail: any;
  public tradeGBP: number = 0;
  public closeWithIndex: number;
  public closeWithPL: string;

  public sortFilter: any;
  public sortFilter1: any;

  private isDestroy: boolean;
  private xhmTimeout: any;

  // **INTERVAL DECLRATION**/
  subscription: Subscription;
  statusText: string;
  private isUpdatedIndex: number = -1;
  public stopLossValue: number;
  public takeProfitValue: number;

  public balance: number = 0;
  public margin: number = 0;
  public marginTMP: number = 0;
  public freemargin: number = 0;
  public freemarginTMP: number = 0;
  private wsConnection: boolean = false;

  public orderTypeOpnList: any;
  public productTypeOpnList: any;
  public retentionTypeOpnList: any;
  public productOption: string;
  public retentionOption: string;
  public orderOption: string;

  oldPrice: number;
  oldTrigger: number;
  select: any;
  modelId : number = 1;
  isActiveCnfm: boolean;
  isActive: boolean;
  type = '';
  collapseOpen = -1;
  collapse1 = '';
  constructor(
    inj: Injector,
    private modalService: ModalService,
    private service: AuthService,
    private elRef: ElementRef,
    private shareData: SharedataService,
    // private socketService: SocketService,
    public CONSTANT: ConstantsService,
    private renderer: Renderer2,
    private cHTML: cHtmlHelper) {
    super(inj);
    // this.orderOption = CONSTANT.DEFUALT_ORDER_OPTION;
    this.isDestroy = false;
    this.isActiveCnfm = false;
    this.isActive = false;
    isweekend = this.isWeekend(this.getTodayDate());
  }

  async ngOnInit() {
    await this.initData();

    await this.initBalance(); // GET CURRANT BALANCE
    this.initTradOther(); // MARGIN
    this.initTrade(); //  CURRANT TRADE
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    // this.socketService.close();
    // this.socketService.messages.unsubscribe();
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    clearTimeout(this.xhmTimeout);
  }

  initBalance() {
    return new Promise((resolve, reject) => {
      this.service.headerCall().subscribe((res) => {
        this.balance = res.data.balance.balance
        resolve(true)
      })
    })

  }

  initTrade() {
    this.service.getTradePosition().subscribe((res) => {
      this.tradePositionList = [];
      this.tradeOrdersList = [];
      if (res.status === 1) {
        res.data.forEach(item => {
          const cData = new Trade(item);
          if (cData.status === 1) {
            this.tradePositionList.push(cData);
          } else {
            this.tradeOrdersList.push(cData);
          }
        });
        // clearTimeout(this.xhmTimeout);
        // this.socketService.close(); // CLOSE CONNECTION
        // this.getLiveQuote1();
        if (!isweekend) {
          this.initWsConnection();
        } else {
          this.getLiveQuote1();
        }
      }

      if (!this.isDestroy) {
        const xhm1 = setTimeout(() => {
          this.initTrade();
          clearTimeout(xhm1);
          clearTimeout(this.xhmTimeout);
          // this.socketService.close(); // CLOSE CONNECTION
        }, 3000);
      }
    });
  }

  private initWsConnection(): void {
    this.getLiveQuote1();
    //  this.socketService.messages.subscribe(res => {
    //      // console.log("Response from websocket: ", res);
    //      if (res.length) {
    //        this.wsConnection = true;
    //        if (!this.dapthArr.length) {
    //          res.forEach((item) => {
    //            this.dapthArr[item.name] = item.depth;
    //          });
    //        } else if (this.dapthArr.length === 3) {
    //          res.forEach((item) => {
    //            this.dapthArr[item.name] = item.depth;
    //          });
    //        }
    //        this.bindTradeLive(this.dapthArr);
    //        this.bindOrderTrade(this.dapthArr);
    //        const tThis = this;
    //        const clickButtons = this.elRef.nativeElement.querySelectorAll('.order-sec');
    //        for (let i = 0; i < clickButtons.length; i++) {
    //          this.renderer.listen(clickButtons[i], 'click', ($event) => {
    //            tThis.openModal('custom-modal-1', clickButtons[i].dataset);
    //          });
    //        }
    //      } else {
    //        this.getLiveQuote1();
    //      }
    //    },
    //    (error) => {
    //      console.log('error', error);
    //      this.wsConnection = false;
    //      this.getLiveQuote1();
    //      this.socketService.close();
    //      this.initWsConnection();
    //    },
    //    () => {
    //      console.log('complete');
    //      this.socketService.close();
    //    });
  }

  getLiveQuote1() {
    if (this.tradePositionList.length || this.tradeOrdersList.length) {
      this.service.getLiveMarketDepth()
        .subscribe(res => {
          if (res.status === 1 || res.status === 'success') {
            if (res.status === 'success') {
              const bepthA = [];
              Object.keys(res.data).map(function (data) {
                const cData = res.data[data];
                const depthItem = { 'buy': cData.depth.buy[0].price, 'sell': cData.depth.sell[0].price };
                bepthA[cData.name] = depthItem;
              });
              this.dapthArr = bepthA;
            } else {
              // res.data.forEach((item) => {
              //   const depthItem = {'buy': JSON.parse(item.depth).buy[0].price, 'sell': JSON.parse(item.depth).sell[0].price};
              //   this.dapthArr[item.name] = depthItem;
              // });
              this.dapthArr = res.data;
            }
            this.bindTradeLive(res.data);
            this.bindOrderTrade(this.dapthArr);
            const tThis = this;
            const clickButtons = this.elRef.nativeElement.querySelectorAll('.order-sec');
            const clickButtons1 = this.elRef.nativeElement.querySelectorAll('.delete-order');
            const clickButtons2 = this.elRef.nativeElement.querySelectorAll('.update-order');
            const clickButtons5 = this.elRef.nativeElement.querySelectorAll('.openCollapse');

            // const clickButtons3 = this.elRef.nativeElement.querySelectorAll('.close-inline-edit');
            // const clickButtons4 = this.elRef.nativeElement.querySelectorAll('.update-inline-edit');

            for (let i = 0; i < clickButtons5.length; i++) { // collapse open
              this.renderer.listen(clickButtons5[i], 'click', ($event) => {
                // tThis.openInlineEdit(clickButtons5[i].dataset);
                if (this.collapseOpen === clickButtons5[i].dataset.index) {
                  this.collapseOpen = -1;
                } else {
                  this.collapseOpen = clickButtons5[i].dataset.index;
                }
              });
            }
            const clickButtons6 = this.elRef.nativeElement.querySelectorAll('.squareOff');
            for (let i = 0; i < clickButtons6.length; i++) {
              this.renderer.listen(clickButtons6[i], 'click', ($event) => {
                tThis.openModal('modal-3', clickButtons6[i].dataset);
              });
            }

            const clickButtons7 = this.elRef.nativeElement.querySelectorAll('.modify');
            for (let i = 0; i < clickButtons7.length; i++) {
              this.renderer.listen(clickButtons7[i], 'click', ($event) => {
                tThis.openModal('modal-1', clickButtons7[i].dataset);
              });
            }

            for (let i = 0; i < clickButtons.length; i++) {
              this.renderer.listen(clickButtons[i], 'click', ($event) => {
                tThis.openModal('custom-modal-1', clickButtons[i].dataset);
              });
            }
            if (this.tradeOrdersList.length) {
              for (let i = 0; i < clickButtons1.length; i++) {
                this.renderer.listen(clickButtons1[i], 'click', ($event) => {
                  tThis.deleteOrder(clickButtons1[i].dataset);
                });
                for (let i = 0; i < clickButtons2.length; i++) {
                  this.renderer.listen(clickButtons2[i], 'click', ($event) => {
                    tThis.openModal('modal-2', clickButtons2[i].dataset);
                  });
                }
              }
            }
            // for (let i = 0; i < clickButtons2.length; i++) {
            //   this.renderer.listen(clickButtons2[i], 'click', ($event) => {
            //     tThis.openModal('modal-2', clickButtons2[i].dataset);
            //   });
            // }
            // for (let i = 0; i < clickButtons2.length; i++) { // update SL AND TP
            //   this.renderer.listen(clickButtons2[i], 'click', ($event) => {
            //     tThis.openInlineEdit(clickButtons2[i].dataset);
            //   });
            // }
            // for (let i = 0; i < clickButtons3.length; i++) { // update SL AND TP
            //   this.renderer.listen(clickButtons3[i], 'click', ($event) => {
            //     tThis.closeInlineEdit();
            //   });
            // }
            // for (let i = 0; i < clickButtons4.length; i++) { // update SL AND TP
            //   this.renderer.listen(clickButtons4[i], 'click', ($event) => {
            //     tThis.updateSLTP();
            //   });
            // }
          }
          if (!this.isDestroy && !this.wsConnection && !isweekend) {
            this.xhmTimeout = setTimeout(() => {
              clearTimeout(this.xhmTimeout);
              this.getLiveQuote1();
            }, 1005);
          }
        });
    }
  }

  bindTradeLive(res) {
    if (this.isUpdatedIndex !== -1) {
      return false;
    }
    let htmlData = '';                      // Position
    htmlData += '<h2 class="sub-title"> <div class="col-md-12 row"><div class="col-md-6 col-6"><b>SYMBOL</b></div>';
    htmlData += '<div class="col-md-6 col-6 text-right"><b>MTM</b></div></div><div class="col-md-12 text-right">Booked P/L</div>';
    htmlData += '<div class="col-md-12 row"><div class="col-md-6 col-6">Product | LTP</div>';
    // htmlData += '<div class="col-md-6 col-6 text-right">Net Qty</div>';
    htmlData += '</div></h2>';
    htmlData += '<div class="table-responsive">';
    htmlData += '<table class="table table-tposition bg-card">';
    htmlData += '<tbody>';
    this.tradeGBP = 0;
    this.tradePositionList.forEach((item, index) => {
      const item_key = `${item.name}_${item.instrumentToken}`
      if (res[item_key]) {
        let BUY_SELL_PRICE = item.trans_type === 'BUY' ? item.buy_price : item.sell_price;
        let profitLoss = (res[item_key].buy - BUY_SELL_PRICE) * this.lotCal(item.lot, item.name);
        let rateDiff = BUY_SELL_PRICE + ' <i class="fa fa-long-arrow-right"></i> ' + `${res[item_key].buy}`;
        
        if (item.trans_type === 'SELL') {
          if(item.ot_type != 'SL'){ BUY_SELL_PRICE = item.ot_type === 'LIMIT' ? item.price : item.sell_price ; }
          else{ BUY_SELL_PRICE = item.price}
          rateDiff = BUY_SELL_PRICE + ' <i class="fa fa-long-arrow-right"></i> ' + res[item_key].buy;
          profitLoss = (BUY_SELL_PRICE - res[item_key].buy) * this.lotCal(item.lot, item.name);
        }

        if (item.trans_type === 'BUY') {
          if(item.ot_type != 'SL'){  BUY_SELL_PRICE = item.ot_type === 'LIMIT' ? item.price : item.buy_price ; }
          else{ BUY_SELL_PRICE = item.price}
          rateDiff = BUY_SELL_PRICE + ' <i class="fa fa-long-arrow-right"></i> ' + res[item_key].sell;
          profitLoss = (res[item_key].sell - BUY_SELL_PRICE) * this.lotCal(item.lot, item.name);
        }

        if (!isNaN(profitLoss)) {
          const tmp = this.tradeGBP + profitLoss;
          this.tradeGBP = this.cHTML.parseFloat2Decimals(tmp, 4);
        }

        let color = '';
        let text = '';
        if (profitLoss < 0) {
          color = 'red';
          text = 'LOSS ';
        } else {
          color = 'golden';
          text = 'PROFIT ';
        }

        if (this.modelId === this.tradePositionList[index].status) {
          if (this.closeWithIndex === index) { // CLOSE WITH PL TO RATE CHANGE
            this.orderDetail.buyCPrice = res[item_key].sell;
            this.orderDetail.sellCPrice = res[item_key].buy;
            this.orderDetail.profit = this.cHTML.parseFloat2Decimals(profitLoss, 2);
          }
        }

        this.tradePositionList[index].profit = profitLoss;
        htmlData += '<tr data-toggle="collapse" class="openCollapse" href="#data-' + item.id + '" data-index="' + index + '">';
        htmlData += '<td class="col-md-12 col-12 row">';
        htmlData += '<div class="text-left col-md-6 col-6">';
        htmlData += '<b>' + item.name + ',</b> <span class="' + color + '">' + item.trans_type + ' ' + item.lot + '</span>';
        // htmlData += '<p>' + `${rateDiff}` + '</p>';
        htmlData += '<p><small>' + item.tradingSymbol + '</small></p>';

        htmlData += '<p class="update-sltp show" data-oindex="' + item.id + '" data-ostoploss="' + item.stopLoss + '" data-otakeprofit="' + item.takeProfit + '"><span class="mr-2 text-uppercase"> ' + item.product_type + ' </span> | <span  class="ml-2 text-uppercase">' + BUY_SELL_PRICE + '</span></p>';
        htmlData += '<p id="sltp-form-' + item.id + '"></p></div>';

        htmlData += '<div class="text-right col-md-6 col-6">';
        htmlData += '<b class="' + color + '">' + this.cHTML.parseFloat2Decimals(profitLoss, 3) + '</b>';
        htmlData += '<p><small class="' + color + '">' + this.cHTML.parseFloat2Decimals(profitLoss, 3) + '</small></p>';
        // htmlData += '<p> 0 </p>';
        htmlData += '</div>';

        let collapse1;
        if (this.collapseOpen != index) {
          collapse1 = 'collapse'
        }

        htmlData += '<div id="data-' + item.id + '" class="' + collapse1 + ' col-md-12 col-12 row">';
        htmlData += '<div class="row col-sm-12"><div class="col-sm-6 col-6"><a class="btn btn-buy btn-sm col-sm-4 btn-outline-warning modify" data-index="' + index + '">MODIFY</a></div>';
        htmlData += '<div class="col-sm-6 col-6"><a class="btn btn-buy btn-sm col-sm-4 btn-outline-warning squareOff" data-index="' + index + '">SQUARE OFF</a></div></div>';

        htmlData += '</td></tr>';
      }
    });
    htmlData += '</tbody></table></div>';

    if (this.isActiveCnfm != true && this.isActive != true) {
      this.positionRef.nativeElement.innerHTML = htmlData;
    }
    // debugger;
    if (this.tradeGBP < 0) {
      this.margin = this.marginTMP + this.tradeGBP;
    } else {
      this.freemargin = this.freemarginTMP + this.tradeGBP;
    }
  }

  bindOrderTrade(res) {
    let htmlData = '';
    if (this.tradeOrdersList.length) {
      htmlData += '<h2 class="sub-title">Orders</h2>';
      htmlData += '<div class="table-responsive">';
      htmlData += '<table class="table table-tposition">';
      htmlData += '<tbody>';
    }
    this.tradeOrdersList.forEach((item, index) => {
      const item_key = `${item.name}_${item.instrumentToken}`
      let rateDiff;
      if(item.product_type === 'CO' || item.ot_type ==='SLM'){
        if(item.ot_type !='LIMIT'){
          rateDiff = item.lot + ' /0.00 at ' + this.cHTML.parseFloat2Decimals(item.trigger_price, 3);
        }else { rateDiff = item.lot + ' /0.00 at ' + this.cHTML.parseFloat2Decimals(item.price, 3);}
      }else{ rateDiff = item.lot + ' /0.00 at ' + this.cHTML.parseFloat2Decimals(item.price, 3);}

      let color = '';
      if (item.trans_type === 'SELL') {
        color = 'red';
      } else {
        color = 'golden';
      }

      if (this.modelId === this.tradeOrdersList[index].status) {
      if (this.closeWithIndex === index) { // CLOSE WITH PL TO RATE CHANGE
        this.orderDetail.buyCPrice = res[item_key].buy;
        this.orderDetail.sellCPrice = res[item_key].sell;
      }
    }

      htmlData += '<tr>';
      htmlData += '<td class="text-left" id="order-new-' + item.id + '">';
      htmlData += '<b>' + item.name + ',</b> <span class="' + color + '">' + this.cHTML.convertToTitle(item.ot_type) + '</span>';
      htmlData += '<p>' + `${rateDiff}` + '</p>';
      htmlData += '</td><td class="text-right">';
      htmlData += '<b class="red">Placed</b>';
      htmlData += '<button class="btn btn-sm btn-outline-warning ml-2 update-order" data-index="' + index + '">Update</button>';
      htmlData += '<button class="btn btn-sm btn-outline-danger ml-2 delete-order" data-oid="' + item.id + '">Delete</button></td></tr>';
    });
    if (htmlData !== '') {
      htmlData += '</tbody></table></div>';
    }
    if (this.isActiveCnfm != true && this.isActive != true) {
      this.ordersRef.nativeElement.innerHTML = htmlData;
    }
  }

  initTradOther() {
    // this.freemargin = 0;
    this.service.getTradeDetail().subscribe((res) => {
      if (res.status === 1 && res.data) {
        this.margin = res.data.margin;
        this.marginTMP = res.data.margin;

        this.freemargin = (this.balance - this.marginTMP);
        this.freemarginTMP = (this.balance - this.marginTMP);
      }
    });
  }


  openModalOrder(id: string, i?: any) {
    if (id === 'custom-modal-1') {
      this.closeWithIndex = Number(i.index);
      this.initOrders(this.tradePositionList[i.index], this.lotArr[this.tradePositionList[i.index].name], this.orderOption);
    }
    this.modalService.open(id);
  }

  openModal(id: string, i?: any) {
    if (id === 'modal-1') {
      this.modelId = 1; 
      this.isActive = true;
      this.closeWithIndex = Number(i.index);
      // this.type = "SELL";
      // this.orderOption = 'MARKET';
      // this.retentionOption = 'DAY';
      // this.productOption = 'NRML';
      // this.isOrderIndex = Number(data.index);
      this.initOrder(this.tradePositionList[i.index]);
    }
    if (id === 'modal-2') {
      this.modelId = 3;
      this.isActive = true;
      this.closeWithIndex = Number(i.index);
      this.initOrder(this.tradeOrdersList[i.index]);
    }

    if (id === 'modal-3') {
      // this.isActive = true;
      this.modelId = 1;
      this.isActiveCnfm = true;
      this.closeWithIndex = Number(i.index);
      this.initOrder(this.tradePositionList[i.index]);
      this.confirmModel()
    }

    if (id === 'custom-modal-1') {
      this.closeWithIndex = Number(i.index);
      this.initOrder(this.tradePositionList[i.index]);
      this.modalService.open(id);
    }
    if (id === 'custom-modal-2') {
      this.closeWithIndex = Number(i.index);
      this.orderDetail = this.initOrders(i, this.orderOption, this.dapthArr);
      this.modalService.open(id);
    }
    if (id === 'custom-modal-3') {
      this.modelId = 3;
      this.closeWithIndex = Number(i.index);
      this.initOrder(this.tradeOrdersList[i.index]);
      this.modalService.open(id);
    }
    // this.modalService.open(id);
  }
  checkCollapse(id) {
    this.collapseOpen = id;
    if (this.collapseOpen === id) {
      this.collapse1 = 'collapse'
    } else {
      this.collapseOpen = -1;
    }
  }

  modelClosed() {
    this.isActive = false;
    this.collapseOpen = -1;
    if (this.isActiveCnfm === true) {
      this.isActiveCnfm = false;
    }
    // this.orderOption = this.CONSTANT.DEFUALT_ORDER_OPTION;
    // this.productOption = this.CONSTANT.DEFUALT_PRODUCT_OPTION;
    // this.retentionOption = this.CONSTANT.DEFUALT_RETENTION_OPTION;
    this.orderDetail = {};
  }

  openSquareOff(i) {
    this.isActive = true;
    // this.isActiveCnfm = true;
    this.closeWithIndex = Number(i);
    this.initOrder(this.tradePositionList[i]);
  }

  confirmModel() {
    this.type = this.orderDetail.trans_type;
    this.isActiveCnfm = true;
    this.orderDetail.ot_type = this.orderOption;
    this.orderDetail.product_type = this.productOption;
    this.orderDetail.retention_type = this.retentionOption;
  }

  editOrder() {
    this.isActiveCnfm = false;
    if(this.isActive === false){
      this.isActive = true;
    }
  }

  closeModal(id: string) {
    if (id === 'custom-modal-1' || id === 'custom-modal-2' || id === 'custom-modal-3') {
      this.modelId = 1;
      this.closeWithIndex = -1;
      this.initORresetOrder();
    }
    this.modalService.close(id);
  }

  initOrder(data) {
    this.orderDetail['profit'] = 0;
    this.orderDetail['buyCPrice'] = '';
    this.orderDetail['sellCPrice'] = '';
    const item_key = `${data.name}_${data.instrumentToken}`

    let profitLoss = (this.dapthArr[item_key].buy - data.buy_price) * this.lotCal(data.lot, data.name);
    // if (data.name === this.CONSTANT.SILVER && data.lot === this.CONSTANT.SILVER_1KG) {
    //   profitLoss = (this.dapthArr[item_key].buy - Number(data.buy_price));
    // }

    if (data.trans_type === this.CONSTANT.TYPE_SELL) {
      profitLoss = (data.sell_price - this.dapthArr[item_key].sell) * this.lotCal(data.lot, data.name);

      // if (data.name === this.CONSTANT.SILVER && data.lot === this.CONSTANT.SILVER_1KG) {
      //   profitLoss = (this.dapthArr[item_key].sell - Number(data.sell_price));
      // }
    }

    this.orderDetail.profit = this.cHTML.parseFloat2Decimals(profitLoss, 2);
    this.orderDetail.name = data.name;
    this.orderDetail.tradingSymbol = data.tradingSymbol;
    this.orderDetail.instrumentToken = data.instrumentToken;
    this.orderDetail.expiry = data.expiry;
    this.orderDetail.sellPrice = data.sell_price;
    this.orderDetail.buyPrice = data.buy_price;
    this.orderDetail.sellCPrice = this.dapthArr[item_key].sell;
    this.orderDetail.buyCPrice = this.dapthArr[item_key].buy;
    this.orderDetail.lot = data.lot;
    this.orderDetail.disc_lot = data.disc_lot;
    this.orderDetail.price = data.price;
    this.orderDetail.position = data.id;
    this.orderDetail.trans_type = data.trans_type;
    this.orderDetail.SL = data.stopLoss;
    this.orderDetail.TP = data.takeProfit;
    this.orderDetail.status = data.status;
    this.orderDetail.trigger_price = data.trigger_price;
    this.orderOption = data.ot_type;
    this.retentionOption = data.retention_type;
    this.productOption = data.product_type;
  }

  initORresetOrder() {
    this.orderDetail = {
      position: '',
      name: '',
      tradingSymbol: '',
      instrumentToken: '',
      expiry: '',
      orderType: '',
      product_type: this.productOption,
      retention_type: this.retentionOption,
      ot_type: this.orderOption,
      lot: '',
      dist_lot: '',
      buyPrice: '',
      sellPrice: '',
      buyCPrice: '',
      sellCPrice: '',
      SL: '',
      TP: '',
      deviation: 0,
      checkPrice: 0,
      price: '',
      status: 0,
      trans_type: '',
      profit: 0,
    };
  }

  initData() {
    this.orderDetail = this.resetOrder();
    this.lotArr = this.initLot();
    this.sellOptions = this.initOrderTimeOptions();
    this.orderOptionList = this.initOrderOption();
    this.productTypeOpnList = this.initProductType();
    this.orderTypeOpnList = this.initOrderType();
    this.retentionTypeOpnList = this.initRetentionType();

    this.sortFilter1 = [{
      'lable': 'Balance',
      'value': 'balance'
    },
    {
      'lable': 'Deposit',
      'value': 'deposit'
    },
    {
      'lable': 'Withdrawal',
      'value': 'withdrawal'
    }];

    this.sortFilter = [{
      'lable': 'Order',
      'value': 'order'
    },
    {
      'lable': 'Time',
      'value': 'time'
    },
    {
      'lable': 'Symbol',
      'value': 'symbol'
    },
    {
      'lable': 'Profit',
      'value': 'profit'
    }];
  }


  orderOptionChange(e) {
    const action = e;
    this.orderOption = e;
    this.orderDetail.orderType = e;

    switch (action) {
      case this.orderOption === 'LIMIT':
        this.orderDetail.trigger_price = 0;
        break;
      case this.orderOption  === 'MARKET':
        this.orderDetail.price = 0;
        this.orderDetail.trigger_price = 0;
        break;
      case this.orderOption === 'SLM':
        this.orderDetail.price = 0;
        break;
    }
    
    // if (this.orderOption === 'LIMIT') {
    //     this.orderDetail.price = this.oldPrice;
    //     if(this.productOption != 'CO'){
    //       this.orderDetail.trigger_price = this.oldTrigger;
    //       this.orderDetail.trigger_price = 0; }
    //   }
    // if (this.orderOption  === 'MARKET') {
    //     this.oldPrice = this.orderDetail.price;
    //     this.orderDetail.price = '';
    //     if(this.productOption != 'CO'){
    //       this.oldTrigger = this.orderDetail.trigger_price;
    //       this.orderDetail.trigger_price = 0; }
    //   }
    // if (this.orderOption === 'SL') {
    //     this.orderDetail.price = this.oldPrice;
    //     this.orderDetail.trigger_price = this.oldTrigger;
    //   }
    // if (this.orderOption === 'SLM') {
    //     this.orderDetail.price = this.oldPrice;
    //     this.orderDetail.trigger_price = this.oldTrigger;
    //     this.orderDetail.price = '';
    //   }

  }
  productOptionChange(e) {
    this.productOption = e;
    this.orderDetail.product_type = e;
  }
  retentionOptionChange(e) {
    this.retentionOption = e;
    this.orderDetail.retention_type = e;
  }

  setSellDay(e) {

  }

  increment(str) {
    switch (str) {
      case this.CONSTANT.DEVIATION:
        this.orderDetail[this.CONSTANT.DEVIATION] += 1;
        break;
      case this.CONSTANT.TP:
        if (this.orderDetail[this.CONSTANT.TP] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.TP] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.TP] += 1;
        break;
      case this.CONSTANT.SL:
        if (this.orderDetail[this.CONSTANT.SL] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.SL] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.SL] += 1;
        break;
      case this.CONSTANT.PRICE:
        if (this.orderDetail[this.CONSTANT.PRICE] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.PRICE] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.PRICE] = +this.orderDetail[this.CONSTANT.PRICE] + 1;
        break;
    }
  }

  decrement(str) {
    if (this.orderDetail[str] <= 0) {
      return false;
    }

    switch (str) {
      case this.CONSTANT.DEVIATION:
        this.orderDetail[this.CONSTANT.DEVIATION] -= 1; // (this.orderDetail['deviation'] - 1);
        break;
      case this.CONSTANT.TP:
        if (this.orderDetail[this.CONSTANT.TP] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.TP] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.TP] -= 1;
        break;
      case this.CONSTANT.SL:
        if (this.orderDetail[this.CONSTANT.SL] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.SL] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.SL] -= 1;
        break;
      case this.CONSTANT.PRICE:
        if (this.orderDetail[this.CONSTANT.PRICE] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.PRICE] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.PRICE] -= 1;
        break;
    }
  }


  placeOrder(frmData, type?) {
    type = '';
    this.modelClosed();

    swal.queue([{
      allowOutsideClick: false,
      allowEscapeKey: false,
      showConfirmButton: false,
      showCloseButton: false,
      showCancelButton: false,
      title: this.CONSTANT.ORDER_QUEUE_TITLE_MSG,
      text: this.CONSTANT.ORDER_QUEUE_TEXT_MSG,
      onOpen: () => {
        swal.showLoading();
        this.service.orderProcessing(frmData, type)
          .subscribe(
            res => {
              swal.hideLoading();
              const type = res.status === 1 ? 'success' : 'error';
              swal.update({
                icon: type,
                title: res.message,
                text: res.data,
              });
              this.shareData.setRouterChange(1); // CALL HEADER
              this.initTrade();
              this.initORresetOrder();
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
              window.location.reload()
            },
            error1 => {
              swal.hideLoading();
              swal.update({
                icon: 'error',
                title: this.CONSTANT.ORDER_REQ_ERROR_TITLE,
                text: this.CONSTANT.ORDER_REQ_ERROR_TEXT,
                showConfirmButton: true,
                timer: this.CONSTANT.SWAL_UPDATE_TIMER
              });
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
            }
          );
      }
    }]);
    this.initBalance()
  }

  changeSorting(type: string) {
    this.tradePositionList = this.sortingArray(this.tradePositionList, type);
    this.bindTradeLive(this.dapthArr);
  }
  update_sl_tp(orderDetail) {
    let data = {
      "SL": orderDetail.SL,
      "TP": orderDetail.TP,
      "orderId": orderDetail.position,
      // "checkPrice": orderDetail.checkPrice
    }

    swal.queue([{
      allowOutsideClick: false,
      allowEscapeKey: false,
      showConfirmButton: false,
      showCloseButton: false,
      showCancelButton: false,
      title: this.CONSTANT.UPDATE_SL_TP_TITLE_MSG,
      text: this.CONSTANT.ORDER_QUEUE_TEXT_MSG,
      onOpen: () => {
        swal.showLoading();
        this.service.update_sl_tp(data)
          .subscribe(
            res => {
              swal.hideLoading();
              swal.update({
                icon: 'success',
                title: res.message,
                text: res.data,
              });
              this.shareData.setRouterChange(1); // CALL HEADER
              this.initTrade();
              this.initORresetOrder();
              this.modelClosed();
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
            },
            error1 => {
              swal.hideLoading();
              swal.update({
                icon: 'error',
                title: this.CONSTANT.ORDER_REQ_ERROR_TITLE,
                text: this.CONSTANT.ORDER_REQ_ERROR_TEXT,
                showConfirmButton: true,
                timer: this.CONSTANT.SWAL_UPDATE_TIMER
              });
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
            });
      }
    }]);
  }

  deleteOrder(data) {
    const htmlTempl = document.getElementById('order-new-' + data.oid);
    swal.fire({
      title: 'Are you sure?',
      // text: 'You won\'t be able to revert this!','
      html: '<p>' + htmlTempl.innerHTML + '</p>',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.deleteOrderCall(data.oid);
      }
    });
  }

  deleteOrderCall(data) {
    this.service.deleteOrder(data).subscribe((res) => {
      if (res.status === 1) {
        swal.fire(
          'Deleted!',
          res.message,
          'success'
        );
        clearTimeout(this.xhmTimeout);
        this.initBalance(); // GET CURRANT BALANCE
        this.initTradOther(); // MARGIN
        this.initTrade(); //  CURRANT TRADE
      } else {
        swal.fire(
          'Error!',
          'Something wan`t wrong. Please try agin..',
          'error'
        );
      }
    });
  }


  update_order(orderDetail) {
    let data = {
      "SL": orderDetail.SL,
      "TP": orderDetail.TP,
      "price": orderDetail.price,
      "orderId": orderDetail.position,
      "trigger_price": orderDetail.trigger_price,
      // "ot_type": orderDetail.ot_type
    }

    // this.closeModal('custom-modal-3');

    swal.queue([{
      allowOutsideClick: false,
      allowEscapeKey: false,
      showConfirmButton: false,
      showCloseButton: false,
      showCancelButton: false,
      title: this.CONSTANT.UPDATE_SL_TP_TITLE_MSG,
      text: this.CONSTANT.ORDER_QUEUE_TEXT_MSG,
      onOpen: () => {
        swal.showLoading();
        this.service.update_order(data)
          .subscribe(
            res => {
              swal.hideLoading();
              swal.update({
                icon: 'success',
                title: res.message,
                // text: res.data,
              });
              this.shareData.setRouterChange(1); // CALL HEADER
              this.initTrade();
              this.initORresetOrder();
              this.modelClosed();
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
            },
            error1 => {
              swal.hideLoading();
              swal.update({
                icon: 'error',
                title: this.CONSTANT.ORDER_REQ_ERROR_TITLE,
                text: this.CONSTANT.ORDER_REQ_ERROR_TEXT,
                showConfirmButton: true,
                timer: this.CONSTANT.SWAL_UPDATE_TIMER
              });
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, this.CONSTANT.SWAL_CLOSE_CLEAR_TIMEOUT);
            });
      }
    }]);
  }

  // bindInlineForm(item) {
  //    let htmlData = '';
  //   htmlData += '<form class="form-inline" (ngSubmit)="updateSLTl()">';
  //   htmlData +=   '<div class="form-group mr-2">';
  //   htmlData +=   '<input type="hidden" name="orderid" id="orderid" value="' + item.id + '">';
  //   htmlData +=   '<label class="mr-2">SL: </label>';
  //   htmlData +=   '<input type="text" name="sl" class="sl-tp" id="sl" value="' + this.stopLossValue + '" placeholder="SL" style="height: 30px; width:90px;">';
  //   htmlData +=   '</div>';
  //   htmlData +=   '<div class="form-group mr-2">';
  //   htmlData +=  '<label class="mr-2">TP: </label>';
  //   htmlData +=   '<input type="text" name="tp" class="sl-tp" id="tp" value="' + this.takeProfitValue + '" placeholder="TP" style="height: 30px; width:90px;">';
  //   htmlData +=  '</div>';
  //   htmlData +=  '<a class="btn btn-secondary mr-2 close-inline-edit" style="height: 30px; padding:0px 10px 0px 10px;"><i class="fa fa-close"></i> </a>';
  //   htmlData +=  '<button type="button" class="btn btn-success update-inline-edit" style="height: 30px;  padding:0px 10px 0px 10px;"><i class="fa fa-save"></i></button></form>';
  //
  //   document.getElementById('sltp-form-' + item.oindex).innerHTML = htmlData;
  // }
  //
  //  openInlineEdit(data: any) {
  //    this.isUpdatedIndex = Number(data.oindex);
  //    this.takeProfitValue = Number(data.otakeprofit);
  //    this.stopLossValue = Number(data.ostoploss);
  //    this.bindInlineForm(data);
  //  }
  //
  //  closeInlineEdit() {
  //    this.isUpdatedIndex = -1;
  //    this.takeProfitValue = 0;
  //    this.stopLossValue = 0;
  //  }
  //
  //  updateSLTP() {
  //    let slValue = document.getElementById('sl').value;
  //    let tpValue = document.getElementById('tp').value;
  //    const orderid = document.getElementById('orderid').value;
  //    console.log(orderid);
  //  }

}
