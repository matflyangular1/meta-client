import {Component, OnInit, Inject, Renderer2} from '@angular/core';
import {ModalService, SharedataService} from '../core/services/index';

@Component({
  selector: 'app-modules',
  templateUrl: './index.component.html',
})
export class ModulesComponent implements OnInit {

  public globleAnnouncement: string;
  public bannerImg: string;

  constructor(private renderer: Renderer2, private shareData: SharedataService, private modalService: ModalService) {
    this.renderer.addClass(document.body, 'hold-transition');
    // this.renderer.addClass(document.body, 'hold-transition');
    this.bannerImg = '';
    if (JSON.parse(localStorage.getItem('currentUser')).banner !== null) {
      this.bannerImg = JSON.parse(localStorage.getItem('currentUser')).banner;
    }

  }

  ngOnInit() {
    this.shareData.globleCommantryChange.subscribe((res) => {
      this.globleAnnouncement = res;
    });

    const xhm = setTimeout(() => {
      clearTimeout(xhm);
      if (localStorage.getItem('isBannershow') === '0') {
        localStorage.setItem('isBannershow', '1');
        this.openModal();
      }
    }, 1000);
  }

  openModal() {
    this.modalService.open('custom-modal-11', 'modal-lg');
  }

  closeModal() {
    localStorage.setItem('isBannershow', '1');
    this.modalService.close('custom-modal-11');
  }

}
