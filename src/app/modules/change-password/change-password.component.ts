import {Component, OnInit} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {AuthService} from '../../core/services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

declare var $;
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
    providers: [AuthService]
})
export class ChangePasswordComponent implements OnInit {

    form: FormGroup;
    errorMsg: string;
    successMsg: string;
    cPasswordType: string = 'password';
    passwordType: string = 'password';

    constructor(
        public _router: Router,
        public _route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private _auth: AuthService,
    ) {
        this.createForm();
    }

    get f() { return this.form.controls; }
    get frmOldpassword() {
        return this.form.get('oldpassword');
    }

    get frmPassword() {
        return this.form.get('password');
    }

    get frmCpassword() {
        return this.form.get('cpassword');
    }

    ngOnInit() {
        this.setDynamicClass();
    }

    setDynamicClass() {
        $('body').addClass('white-bg');
    }

    createForm() {
        this.form = this.formBuilder.group({
            oldpassword: ['', [Validators.required]],
            password: ['', [Validators.required,
                Validators.minLength(6),
                Validators.maxLength(20),
                Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
            cpassword: ['', [Validators.required]]
        });
    }

    submit() {
        this.errorMsg = '';
        this.successMsg = '';
        if (this.form.valid === true) {
            let data = this.form.value;
            this._auth.changePassword(data).subscribe((res) => this.onSuccess(res));
        }
    }

    onSuccess(res) {
        if (res.status === 1) {
            this.successMsg = res.message;
            this.form.reset();
            localStorage.removeItem('currentUser');
            localStorage.removeItem('event');
            localStorage.removeItem('currentUser');localStorage.removeItem('event');;
            sessionStorage.clear();
            document.cookie.split(';').forEach(function (c) {
              document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
            });
            // this._router.navigate(['/auth/']);
        } else {
            this.errorMsg = res.message;
        }
    }

    closeMsg() {
        this.errorMsg = '';
        this.successMsg = '';
    }

    getLocalStorageData(key: string) {
        if (key != undefined) {
            let data = JSON.parse(localStorage.getItem(key));
            return data;
        }
    }

    viewcPassword(type) {
        if (type === 'password') {
            this.cPasswordType = 'text';
        }
        if (type === 'text') {
            this.cPasswordType = 'password';
        }
    }

    viewPassword(type) {
        if (type === 'password') {
            this.passwordType = 'text';
        }
        if (type === 'text') {
            this.passwordType = 'password';
        }
    }


}
