import { Component, OnInit, ElementRef, ViewChild, OnDestroy, Injector } from '@angular/core';
import { BaseComponent } from '../../share/components/common.component';
import { AuthService, ConstantsService } from '../../core/services';
import { History, HistoryObj } from '../../share/models/history';
import * as moment from 'moment';
import { isweekend } from '../trade/trade.component';
import { cHtmlHelper } from '../../share/helper/html.helper'; // Momentjs
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
  providers: [cHtmlHelper]
})
export class HistoryComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('startdate') startdate: ElementRef;
  @ViewChild('enddate') enddate: ElementRef;
  @ViewChild('historyRef') historyRef: ElementRef;
  @ViewChild('orderRef') orderRef: ElementRef;
  @ViewChild('dealRef') dealRef: ElementRef;
  public filter: any = {};
  public filterBtn: string = 'today';
  public filterName: string = '';
  public todayDT: any;
  public sevdayDT: any;
  public thirtydayDT: any;
  public threemonthDT: any;
  public hList: History[] = [];
  public hListTMP: History[] = [];
  public oList: History[] = [];
  public oListTMP: History[] = [];
  public sortFilter: any;
  public isSortFilter: string;
  public isLoad: boolean;
  public profit: number = 0;
  public balance: number = 0;
  public profit1: number = 0;
  public balance1: number = 0;
  public swap: number = 0;
  public filled: number = 0;
  public canceled: number = 0;
  public total: number = 0;
  public activeTab: string = 'position';
  private isDestroy: boolean = false;
  private dapthArr: any;
  private xhmTimeout: any;

  constructor(inj: Injector, private service: AuthService, private CONSTANT: ConstantsService, private cHTML: cHtmlHelper) {
    super(inj);
    this.isSortFilter = 'open_time';
    this.isLoad = false;
  }

  ngOnInit() {
    this.filters('3_month');
    this.filters('30_day');
    this.filters('7_day');
    this.filters('today');
    this.initData();
    this.getHistoryDetail();
  }

  ngOnDestroy() {
    this.isDestroy = true;
  }

  getHistoryDetail() {
    const fData = this.filter;
    this.service.getHistory(fData).subscribe(res => {
      this.hList = [];
      this.oList = [];

      if (res.status === 1 && res.data && res.data.position && res.data.order) {
        res.data.position.forEach((item) => { // CHECK AND SET MODAL DATA
          const cData = new HistoryObj(item);
          this.hList.push(cData);
        });
        res.data.order.forEach((item) => { // CHECK AND SET MODAL DATA
          const cData = new HistoryObj(item);
          this.oList.push(cData);
        });
        this.hListTMP = this.hList;
        this.oListTMP = this.oList;
        this.hList = this.sortingArray(this.hList, this.isSortFilter); // SORTING ARRAY
        if (!this.isLoad) {
          this.getLiveDepth();
        }
        this.bindHistory(this.hList); // BIND DATA
        this.bindHistoryOrder(this.oList); // BIND ORDER/DEALS

      } else {
        console.log('Something wrong..');
        if (!this.isDestroy) {
          const xhm = setTimeout(() => {
            clearTimeout(xhm);
            this.getHistoryDetail();
          }, 1000);
        }
      }
      this.isLoad = true;
      if (!this.isDestroy) {
        const xhm = setTimeout(() => {
          clearTimeout(xhm);
          this.getHistoryDetail();
        }, 5000);
      }
    });
  }

  getLiveDepth() {
    if (this.hList.length) {
      this.service.getLiveMarketDepth()
        .subscribe(res => {
          if (res.status === 1) {
            this.dapthArr = res.data;
          }
          if (!this.isDestroy && this.hList.length) {
            this.xhmTimeout = setTimeout(() => {
              clearTimeout(this.xhmTimeout);
              this.getLiveDepth();
            }, 1005);
          }
        });
    }
  }

  bindHistoryOld(res) {
    this.total, this.canceled, this.profit, this.balance, this.swap, this.filled = 0;
    let htmlData = '';
    htmlData += '<table class="table table-tposition">';
    htmlData += '<tbody>';
    res.forEach((item, index) => {
      this.total++;
      this.profit += item.profit;
      this.balance = this.profit;
      let text = this.CONSTANT.CANCELED;
      if (item.status === 5) {
        this.filled++;
        text = this.CONSTANT.FILLED;
      } else {
        this.canceled++;
      }
      const price = item.trans_type === this.CONSTANT.TYPE_BUY ? item.buy_price : item.sell_price;

      let color = '';
      if (item.profit < 0) {
        color = 'red';
      } else {
        color = 'golden';
      }
      htmlData += '<tr>';
      htmlData += '<td class="text-left">';
      htmlData += '<b>' + item.name + ', </b>';

      if (this.activeTab === this.CONSTANT.HISTORY_TAB_ORDER) {
        htmlData += '<span class="red">' + item.trans_type === this.CONSTANT.TYPE_SELL ? 'buy' : 'sell' + '</span>';
        htmlData += '<p>' + item.lot + ' / ' + item.lot + ' at ' + price + '</p>';
      } else if (this.activeTab === this.CONSTANT.HISTORY_TAB_POSITION) {
        htmlData += '<span class="' + color + '">' + item.trans_type + ' ' + item.lot + '</span>';
        htmlData += '<p>' + price + ' <i class="fa fa-long-arrow-right text-dark align-middle"></i> ' + item.sell_price + '</p>';
      } else if (this.activeTab === this.CONSTANT.HISTORY_TAB_DEAL) {
        htmlData += '<span class="red">' + item.trans_type === this.CONSTANT.TYPE_SELL ? 'buy' : 'sell' + ' out </span>';
        htmlData += '<p>' + item.lot + ' at ' + item.sell_price + '</p>';
      }

      htmlData += '</td>';
      htmlData += '<td class="text-right">';
      htmlData += '<span class="fill-timer">' + item.updated_at;
      if (this.activeTab === this.CONSTANT.HISTORY_TAB_ORDER) {
        htmlData += '<em>' + text + '</em>';
      } else {
        htmlData += '<em class="' + color + '">' + item.profit + '</em>';
      }
      htmlData += '</span>';
      htmlData += '</td></tr>';
    });
    htmlData += '</tbody></table>';
    this.historyRef.nativeElement.innerHTML = htmlData;
    // this.orderRef.nativeElement.innerHTML = htmlData;
    // this.dealRef.nativeElement.innerHTML = htmlData;
  }

  bindHistory(res) {
    this.profit = 0;
    this.balance = 0;
    this.swap = 0;
    let htmlData = '';
    htmlData += '<table class="table table-tposition">';
    htmlData += '<tbody>';
    res.forEach((item, index) => {
      item.profit = this.getProfit(item);
      this.profit = this.cHTML.parseFloat2Decimals(this.profit + item.profit, 3);
      this.balance = this.profit;

      // const price = item.trans_type === this.CONSTANT.TYPE_BUY ? item.buy_price : item.sell_price;
      let price = item.order_type === 'instant_execution' ? item.buy_price : item.price;
      if (item.trans_type === 'SELL') {
        price = item.order_type === 'instant_execution' ? item.sell_price : item.price;
      }
      const item_key = `${item.name}_${item.instrumentToken}`

      let cprice = 0;
      if (this.dapthArr) {
        cprice = this.dapthArr[item_key].buy;
        if (item.trans_type === 'SELL') {
          cprice = this.dapthArr[item_key].sell;
        }
      }

      let color = '';
      if (item.profit < 0) {
        color = 'red';
      } else {
        color = 'golden';
      }
      htmlData += '<tr>';
      htmlData += '<td class="text-left">';
      htmlData += '<b>' + item.name + ', </b>';
      htmlData += '<span class="' + color + '">' + item.trans_type + ' ' + item.lot + '</span>';
      htmlData += '<p>' + price + ' <i class="fa fa-long-arrow-right text-dark align-middle"></i> ' + cprice + '</p>';

      htmlData += '</td>';
      htmlData += '<td class="text-right">';
      htmlData += '<span class="fill-timer">' + item.updated_at;
      htmlData += '<em class="' + color + '">' + this.cHTML.parseFloat2Decimals(item.profit, 3) + '</em>';
      htmlData += '</span>';
      htmlData += '</td></tr>';
    });
    htmlData += '</tbody></table>';
    this.historyRef.nativeElement.innerHTML = htmlData;
  }

  bindHistoryOrder(res) {
    // debugger;
    this.orderRef.nativeElement.innerHTML = '';
    this.dealRef.nativeElement.innerHTML = '';

    this.total = 0;
    this.canceled = 0;
    this.profit1 = 0;
    this.balance1 = 0;
    this.swap = 0;
    this.filled = 0;

    let htmlData = '';
    let htmlDataDeal = '';
    htmlData += '<table class="table table-tposition">';
    htmlData += '<tbody>';
    htmlDataDeal = htmlData;
    res.forEach((item, index) => {
      this.profit1 = this.profit1 + item.profit;
      this.balance1 = this.profit1;
      this.total++;
      let text = this.CONSTANT.CANCELED;
      if (item.status === 5) {
        this.filled++;
        text = this.CONSTANT.FILLED;
      } else {
        this.canceled++;
      }
      const price = item.trans_type === this.CONSTANT.TYPE_BUY ? item.closeBuyPrice : item.closeSellPrice;

      let color = '';
      if (item.profit < 0) {
        color = 'red';
      } else {
        color = 'golden';
      }
      let htmlData1 = '<tr>';
      htmlData1 += '<td class="text-left">';
      htmlData1 += '<b>' + item.name + ', </b>';
      htmlDataDeal += htmlData1;
      htmlData += htmlData1;

      // if (this.activeTab === this.CONSTANT.HISTORY_TAB_ORDER) {
      let str = item.trans_type === this.CONSTANT.TYPE_SELL ? 'buy' : 'sell';
      htmlData += '<span class="red">' + str + '</span>';
      htmlData += '<p>' + item.lot + ' / ' + item.lot + ' at ' + price + '</p>';
      // } else if (this.activeTab === this.CONSTANT.HISTORY_TAB_DEAL) {
      str = item.trans_type === this.CONSTANT.TYPE_SELL ? 'buy out' : 'sell out';
      htmlDataDeal += '<span class="red">' + str + ' </span>';
      htmlDataDeal += '<p>' + item.lot + ' at ' + price + '</p>';
      // }

      const htmlTmp = '</td><td class="text-right"><span class="fill-timer">' + item.updated_at;
      htmlDataDeal += htmlTmp;
      htmlData += htmlTmp;
      // if (this.activeTab === this.CONSTANT.HISTORY_TAB_ORDER) {
      htmlData += '<em>' + text + '</em>';
      // } else {
      htmlDataDeal += '<em class="' + color + '">' + item.profit + '</em>';
      // }
      htmlData += '</span></td></tr>';
      htmlDataDeal += '</span></td></tr>';
    });
    htmlData += '</tbody></table>';
    htmlDataDeal += '</tbody></table>';

    this.orderRef.nativeElement.innerHTML = htmlData;
    this.dealRef.nativeElement.innerHTML = htmlDataDeal;
  }

  getProfit(item) {
    const item_key = `${item.name}_${item.instrumentToken}`

    if (this.dapthArr === undefined || this.dapthArr[item_key] === undefined) {
      return 0;
    }
    let BUY_SELL_PRICE = item.order_type === 'instant_execution' ? item.buy_price : item.price;
    let profitLoss = (this.dapthArr[item_key].buy - BUY_SELL_PRICE) * this.lotCal(item.lot, item.name);
    if (item.name === 'SILVER' && item.lot === '1kg') {
      profitLoss = (this.dapthArr[item_key].buy - Number(BUY_SELL_PRICE));
    }
    if (item.trans_type === 'SELL') {
      BUY_SELL_PRICE = item.order_type === 'instant_execution' ? item.sell_price : item.price;
      profitLoss = (BUY_SELL_PRICE - this.dapthArr[item_key].sell) * this.lotCal(item.lot, item.name);
      if (item.name === 'SILVER' && item.lot === '1kg') {
        profitLoss = (Number(BUY_SELL_PRICE) - this.dapthArr[item_key].sell);
      }
    }
    return profitLoss;
  }

  changeTabs(type: string) {
    this.activeTab = type;
    this.bindHistory(this.hList);
  }

  /**
   * FILTER ON SINGLE CLICK
   * @param search TYPE LIKE TODAY|LAST 7 DAYS|30 DAYS
   */
  filters(search: string) {
    this.filterBtn = search;
    switch (search) {
      case 'today':
        this.filter.start_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.todayDT = this.filter.start_date + '  - ' + this.filter.end_date;
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(1, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '7_day':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(7, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.sevdayDT = this.filter.start_date + '  - ' + this.filter.end_date;
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '30_day':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(30, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.thirtydayDT = this.filter.start_date + '  - ' + this.filter.end_date;
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(90, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.threemonthDT = this.filter.start_date + '  - ' + this.filter.end_date;
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    if (this.isLoad) {
      this.filter.isFirst = 0;
      this.getHistoryDetail();
    }
    if (!this.isLoad) {
      this.filter.isFirst = 1;
    }
  }

  setInputDate(sDate: Date, eDate: Date) {
    if (this.isLoad) {
      setTimeout(() => {
        this.startdate.nativeElement.value = moment(sDate).format(this.CONSTANT.DATE_FORMAT);
        this.enddate.nativeElement.value = moment(eDate).format(this.CONSTANT.DATE_FORMAT);
      }, this.CONSTANT.SET_INPUT_DATE_TIMEOUT);
    }
    return;
  }

  initData() {
    this.sortFilter = [{
      'lable': 'Order',
      'value': 'order'
    },
    {
      'lable': 'Open Time',
      'value': 'open_time'
    },
    {
      'lable': 'Close Time',
      'value': 'close_time'
    },
    {
      'lable': 'Symbol',
      'value': 'symbol'
    },
    {
      'lable': 'Profit',
      'value': 'profit'
    }];
  }

  changeSorting(type: string) {
    if (this.activeTab === 'position') {
      this.hList = this.sortingArray(this.hList, type);
      this.bindHistory(this.hList);
    } else {
      this.oList = this.sortingArray(this.oList, type);
      this.bindHistory(this.oList);
    }
  }

  filterDt(type: string) {

    // this.filter = {}
    this.filter.type = type

    this.getHistoryDetail()

    // this.service.getHistoryType(type, {}).subscribe((res) => this.bindHistory(res));

    // this.filterName = type;
    // if (type === '') {
    //   this.bindHistory(this.hListTMP);
    //   return true;
    // }
    // const tmp = [];
    // this.hListTMP.forEach((item) => {
    //   if (type === item.name) {
    //     tmp.push(item);
    //   }
    // });
    // this.hList = tmp;
    // this.bindHistory(tmp);

  }
}
