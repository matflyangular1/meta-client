import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AuthService, ConstantsService, ExcelService } from '../../core/services';
import * as moment from 'moment'; // Momentjs
import { Statements, StatementsObj } from '../../share/models/statement';

@Component({
  selector: 'app-profit',
  templateUrl: './statements.component.html',
  styleUrls: ['./statements.component.css'],
  providers: [ExcelService]
})
export class StatementsComponent implements OnInit {
  @ViewChild('startdate') startdate: ElementRef;
  @ViewChild('enddate') enddate: ElementRef;
  public filter: any = {};
  public filterBtn: string = '30_day';
  public filterName: string = '';

  public sList: Statements[] = [];
  public sListTMP: Statements[] = [];
  page: number = 1;
  perPage: number = 10;
  total: number;
  totalPages: number;

  constructor(private service: AuthService,
    private CONSTANT: ConstantsService,
    private excel: ExcelService) {
    this.page = 1; // , private excel: ExcelService
    // this.perPage = CONSTANT.PER_PAGE_MAX_RECODE;
  }

  ngOnInit() {
    this.initFilter();
    this.getStatementDetail();
  }

  initFilter() {
    const now = moment();
    const today = now.format(this.CONSTANT.DATE_FORMAT);

    const data = { isFirst: 1 };
    this.filter = data;

    this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
    this.filter.start_date = moment().subtract(30, 'd').format(this.CONSTANT.DATE_FORMAT);

    setTimeout(() => {
      this.startdate.nativeElement.value = this.filter.start_date;
      this.enddate.nativeElement.value = this.filter.end_date;
    }, 500);


  }

  getStatementDetail() {
    const fData = this.filter;
    this.total = undefined
    this.service.getStatements(fData, this.page).subscribe(res => {
      this.sList = [];
      if (res.status === 1 && res.data !== null) {
        res.data.forEach((item) => { // CHECK AND SET MODAL DATA
          const cData = new StatementsObj(item);
          this.sList.push(cData)
        });
        this.totalPages = res.total

      }
    });
  }

  mudouPagina(event) {
    this.page = event.valor;
    this.getStatementDetail()
  }

  filterByDate() {
    this.filter.start_date = this.startdate.nativeElement.value
    this.filter.end_date = this.enddate.nativeElement.value
    this.getStatementDetail();
  }

  /**
   * FILTER ON SINGLE CLICK
   * @param search TYPE LIKE TODAY|LAST 7 DAYS|30 DAYS
   */
  filters(search: string) {
    this.filterBtn = search;
    this.page = 1
    switch (search) {
      case 'today':
        this.filter.start_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(1, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '7_day':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(7, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '30_day':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(30, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month':
        this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
        this.filter.start_date = moment().subtract(90, 'd').format(this.CONSTANT.DATE_FORMAT);
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    this.filter.isFirst = 0;
    this.getStatementDetail();
  }

  setInputDate(sDate: Date, eDate: Date) {
    setTimeout(() => {
      this.startdate.nativeElement.value = moment(sDate).format(this.CONSTANT.DATE_FORMAT);
      this.enddate.nativeElement.value = moment(eDate).format(this.CONSTANT.DATE_FORMAT);
    }, this.CONSTANT.SET_INPUT_DATE_TIMEOUT);
    return;
  }

  generateAndDownloadExcel() {
    const tmpData = [];
    const tmp = {
      ID: '',
      CREATE_ON: '',
      TYPE: '',
      DESCRIPTION: '',
      DEBIT: '',
      CREADIT: '',
      BALANCE: '',
      CLOSEBUYPRICE: '',
      CLOSESELLPRICE: '',
      COMMITION: ''
    };
    tmpData.push(tmp);
    this.sListTMP.forEach((item) => {
      const credit = item.type === 'CREDIT' ? item.amount : '0.00';
      const debit = item.type === 'DEBIT' ? item.amount : '0.00';
      let desc = item.description;
      if (item.status !== 1) {
        desc += '  ( Canceled )';
      }
      const tmp = {
        ID: item.id,
        TYPE: item.trans_type,
        DESCRIPTION: item.description,
        DEBIT: Number(debit),
        CREADIT: Number(credit),
        BALANCE: item.balance,
        CLOSEBUYPRICE: item.closeBuyPrice,
        CLOSESELLPRICE: item.closeSellPrice,
        COMMITION: item.commission,
        CREATE_ON: item.created_on
      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'statements');
  }

}
