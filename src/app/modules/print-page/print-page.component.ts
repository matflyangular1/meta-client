import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { ConstantsService } from 'src/app/core/services/constants.service';
import * as moment from 'moment'; // Momentjs

@Component({
  selector: 'app-print-page',
  templateUrl: './print-page.component.html',
  styleUrls: ['./print-page.component.css']
})
export class PrintPageComponent implements OnInit, AfterViewInit {
  @ViewChild('startdate') startdate: ElementRef;
  @ViewChild('enddate') enddate: ElementRef;


  cList: any = [];
  dList: any = {};
  filter: any = {};

  constructor(
    private service: AuthService,
    private CONSTANT: ConstantsService
  ) { }

  ngOnInit() {
    this.initFilter();
    this.getPrintStatement();
  }

  ngAfterViewInit() {
    // window.print();
  }

  initFilter() {
    const now = moment();
    const today = now.format(this.CONSTANT.DATE_FORMAT);

    this.filter.end_date = moment().format(this.CONSTANT.DATE_FORMAT);
    this.filter.start_date = moment().subtract(90, 'd').format(this.CONSTANT.DATE_FORMAT);


    setTimeout(() => {
      this.startdate.nativeElement.value = this.filter.start_date;
      this.enddate.nativeElement.value = this.filter.end_date;
    }, 500);
  }


  getPrintStatement() {
    const fData = this.filter;
    this.service.getPrintStatement(fData).subscribe(res => {
      if (res.status === 1 && res.data !== null) {
        this.cList = res.data;
        this.dList = res;
      }
    });
  }

  filterByDate() {
    this.filter.start_date = this.startdate.nativeElement.value
    this.filter.end_date = this.enddate.nativeElement.value
    this.getPrintStatement();
  }

  print() {
    window.print();
  }
}
