import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModulesComponent } from './index.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from '../core/include/header/header.component';
import { FooterComponent } from '../core/include/footer/footer.component';
import { TradeComponent } from './trade/trade.component';
import { HistoryComponent } from './history/history.component';

import { ModalComponent } from '../share/components/modal.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ProfileComponent } from './profile/profile.component';
import { StatementsComponent } from './statements/statements.component';
import { PaginationComponent } from '../share/components/pagination.component';
import { RulesComponent } from './rules/rules.component';
import { CalculationsComponent } from './calculations/calculations.component';
import { NewsComponent } from './news/news.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NewRulesComponent } from './new-rules/new-rules.component';
import { FilterPipe } from '../share/pipes/filter.pipe';
import { ComplainComponent } from './complain/complain.component';
import { ComplainCreateComponent } from './complain/complain-create/complain-create.component';
import { PrintPageComponent } from './print-page/print-page.component';
import { ShareMarketComponent } from './dashboard/share-market/share-market.component';
import { CommodityMarketComponent } from './dashboard/commodity-market/commodity-market.component';
import { DragDropModule} from '@angular/cdk/drag-drop';
import { OrderBookComponent } from './order-book/order-book.component';

const routes: Routes = [
  {
    path: '',
    component: ModulesComponent,
    children: [
      {
        path: 'market',
        component: DashboardComponent,
        children: [
          {
            path: 'commodity',
            component: CommodityMarketComponent
          },
          {
            path: 'share',
            component: ShareMarketComponent
          },
          {path:'',redirectTo:'commodity'}
        ]
      },
      {
        path: 'order',
        component: TradeComponent
      },
      {
        path: 'order-book',
        component: OrderBookComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'statement',
        component: StatementsComponent
      },
      {
        path: 'rules',
        component: RulesComponent
      },
      {
        path: 'ac/profile',
        component: ProfileComponent
      },
      {
        path: 'ac/change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'new-rules',
        component: NewRulesComponent
      },
      {
        path: 'calculations',
        component: CalculationsComponent
      },
      {
        path: 'news',
        component: NewsComponent
      },
      {
        path: 'complain',
        component: ComplainComponent
      },
      {
        path: 'complain-create',
        component: ComplainCreateComponent
      },
      {
        path: 'print',
        component: PrintPageComponent
      },
      {
        path: 'about-us',
        component: AboutUsComponent
      },
      { path: '', redirectTo: 'market', pathMatch: 'full' },
    ]
  },
  {
    path: '**', redirectTo: '/auth'
  }
];

@NgModule({
  declarations: [
    ModulesComponent,
    ModalComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    TradeComponent,
    HistoryComponent,
    ChangePasswordComponent,
    ProfileComponent,
    StatementsComponent,
    PaginationComponent,
    RulesComponent,
    CalculationsComponent,
    NewsComponent,
    AboutUsComponent,
    NewRulesComponent,
    FilterPipe,
    ComplainComponent,
    ComplainCreateComponent,
    PrintPageComponent,
    ShareMarketComponent,
    CommodityMarketComponent,
    OrderBookComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ModulesModule {
}
