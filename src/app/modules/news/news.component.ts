import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services';

interface newsData {
  id: number,
  title: string,
  information: string,
  status: number
}

class newsObj implements newsData {
  id: number;
  title: string;
  information: string;
  status: number
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsComponent implements OnInit {

  public data: newsData[] = [];
  public nList: newsData[] = [];
  isEmpty = false;
  isData = true;
  page = { start: 1, end: 5 };

  constructor(
    private service: AuthService
  ) { }

  ngOnInit() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        }
      });
    }
    this.getNewsList();
  }

  toggleAccordian(event, index) {
    var element = event.target;
    element.classList.toggle("active");
    if (this.nList) {
      this.isData = false;
    } else {
      this.isData = true;
    }
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }

  getNewsList() {
    const fData = this.data;
    this.service.getNewsList().subscribe((res) => {

      if (res.status === 1 && res.data) {
        const items = res.data;
        const data: newsData[] = [];

        if (items.length > 0) {
          this.nList = [];

          for (const item of items) {
            const cData: newsData = new newsObj();

            cData.id = item.id;
            cData.title = item.title;
            cData.information = item.information;
            cData.status = item.status;

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        this.nList = data;
      }
    });
  }
}