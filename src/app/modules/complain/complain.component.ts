import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

declare var $ ;
@Component({
  selector: 'app-complain',
  templateUrl: './complain.component.html',
  styleUrls: ['./complain.component.css']
})
export class ComplainComponent implements OnInit {

  filter: any = {};
  cList: [];
  viewList :any = {};

  page: number = 1;
  perPage: number = 10;
  total: number;
  totalPages: number;
  searchText : string;

  constructor(
    private service: AuthService,
  ) {
    this.page = 1;
   }

  ngOnInit() {
    this.getComplainDetail();
  }

  getComplainDetail() {
    // const fdata = { isFirst: 1 };
    this.service.getComplain(this.page).subscribe(res => {
      this.cList = [];
      if (res.status === 1 && res.data !== null) {

        this.cList = res.data;
      }
      this.totalPages = res.data.length
    });
  }

  mudouPagina(event) {
    this.page = event.valor;
    this.getComplainDetail();
  }

  viewDetail(item){
    console.log(item)
    this.viewList = item ;
    $('.clear-settlement').modal('show');
    $('body').removeClass('modal-open');
  }
}
