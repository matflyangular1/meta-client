import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { HttpEventType, HttpResponse, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-complain-create',
  templateUrl: './complain-create.component.html',
  styleUrls: ['./complain-create.component.css']
})
export class ComplainCreateComponent implements OnInit {

  frm: FormGroup
  percentDone: number;

  error: string;
  fileUpload = { status: '', message: '', filePath: '' };

  fileToUpload: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private service: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  upload(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['',[ Validators.required,]],
      information: ['',[ Validators.required]],
    });
  }


  submitForm() {

    if (this.frm.valid) {
      var data = new FormData();
      
      data.append("input_img", this.fileToUpload);
      data.append("title", this.frm.value.title);
      data.append("information", this.frm.value.information);
      this.service.createComplain(data).subscribe(res => this.onSuccess(res))
    }

  }

  onSuccess(res) {
    this.fileUpload = res
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/complain']);
    }
  }

  get frmTitle() { return this.frm.get('title'); }
  get frmInformation() { return this.frm.get('information'); }
}
