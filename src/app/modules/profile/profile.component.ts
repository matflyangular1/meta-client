import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../core/services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public aList: any;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.getUserDetail();
  }

  getUserDetail() {
    this.auth.headerCall().subscribe((res) => this.onSuccessGetUserDt(res));
  }

  onSuccessGetUserDt(res) {
    // debugger;
    if (res.status === 1) {
      if (res.data !== undefined) {
        res.data.balance.mywallet = this.numberWithCommas(res.data.balance.mywallet);
        res.data.balance.balance = this.numberWithCommas(res.data.balance.balance);
        res.data.balance.expose = this.numberWithCommas(res.data.balance.expose);
        this.aList = res.data;
      }
    }
  }

  numberWithCommas(x) {
    x = String(x).toString();
    let afterPoint = '';
    if (x.indexOf('.') > 0) {
      afterPoint = x.substring(x.indexOf('.'), x.length);
    }
    x = Math.floor(x);
    x = x.toString();
    let lastThree = x.substring(x.length - 3);
    const otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') {
      lastThree = ',' + lastThree;
    }
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree + afterPoint;
  }

}
