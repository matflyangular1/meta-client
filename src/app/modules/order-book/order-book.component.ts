import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services';
import swal from 'sweetalert2';

@Component({
  selector: 'app-order-book',
  templateUrl: './order-book.component.html',
  styleUrls: ['./order-book.component.css']
})
export class OrderBookComponent implements OnInit {

  public filterBtn: string = 'All';
  public sList: any[] = [
    {id:1,name : 'GOLD' , date:"2020-10-12" , type:"MIS", pend: 0 , comp:1, price:410.15 },
    {id:2,name : 'USDINR' , date:"2020-10-12" , type:"LIS", pend: 0 , comp:1, price:410.15 },
    {id:3,name : 'SILVER' , date:"2020-10-12" , type:"MIS", pend: 0 , comp:1, price:410.15 }
  ];
  public dList: any[] =[] ;

  constructor(
    private service : AuthService
  ) { }

  ngOnInit() {
    this.getOrderBook(this.filterBtn);
  }

  getOrderBook(fData) {
    this.filterBtn  = fData;
    this.service.getOrderBook( {type :this.filterBtn} ).subscribe(res => {
      this.dList = res.data
    });
  }

  
  cancleOrder(data) {
    swal.fire({
      title: 'Are you sure?',
      // text: 'You won\'t be able to revert this!','
      html: '<p>' + data.name + '</p>',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancle it!'
    }).then((result) => {
      if (result.value) {
        this.cancleOrderCall(data.id);
      }
    });
  }

  cancleOrderCall(id) {
    this.service.cancleOrder({id:id}).subscribe((res) => {
      if (res.status === 1) {
        swal.fire(
          'Cancled!',
          res.message,
          'success'
        );
        this.getOrderBook(this.filterBtn);
      } else {
        swal.fire(
          'Error!',
          'Something wan`t wrong. Please try agin..',
          'error'
        );
      }
    });
  }

}
