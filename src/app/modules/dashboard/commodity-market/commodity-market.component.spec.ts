import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityMarketComponent } from './commodity-market.component';

describe('CommodityMarketComponent', () => {
  let component: CommodityMarketComponent;
  let fixture: ComponentFixture<CommodityMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
