import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareMarketComponent } from './share-market.component';

describe('ShearMarketComponent', () => {
  let component: ShareMarketComponent;
  let fixture: ComponentFixture<ShareMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
