import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Renderer2, Injector } from '@angular/core';
import { BaseComponent } from '../../../share/components/common.component';
import { ModalService, AuthService, SharedataService, SocketService, WebsocketService, ConstantsService, ScriptLoaderService } from '../../../core/services/index';
import { Quotes, QuotesList } from '../../../share/models/quotes';
import { Subject, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import swal from 'sweetalert2';

declare var $;
export let isweekend = false;
@Component({
  selector: 'app-share-market',
  templateUrl: './share-market.component.html',
  styleUrls: ['./share-market.component.css']
})
export class ShareMarketComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('quoteRef') quoteRef: ElementRef;
  @ViewChild('sellRate') sellRate: ElementRef;
  @ViewChild('buyRate') buyRate: ElementRef;
  public isModalOpen: boolean = false;
  public qList: QuotesList[] = [];
  public qListTMP: QuotesList[] = [];
  public qAdd: QuotesList[] = [];
  public q_tmp: QuotesList[] = [];
  public orderOptionList: any;
  public sellOptions: any;
  public lotArr: any;
  public liveData: any;
  public orderDetail: any;
  private isDestroy: boolean;
  private isLoadFirst: boolean;
  private isOrderIndex: number;
  private isLoadDragJs: boolean = true;

  public orderTypeOpnList: any;
  public productTypeOpnList: any;
  public retentionTypeOpnList: any;
  public productOption: string;
  public retentionOption: string;
  public orderOption: string;
  public buySellOpen: number = -1;

  collpaseMap = new Map();

  modelClick: string;
  searchText = '';
  lastIndex = 0;
  lastPrice = {
    sell: 0,
    buy: 0
  }
  type = '';
  doneList: [];
  arr: {};
  isData = true;
  isActive: boolean;
  isActiveCnfm: boolean;
  options = {
    floor: 0,
    ceil: 0,
    unit: '',
    buttonsConfig: []
  };

  // **INTERVAL DECLRATION**/
  subscription: Subscription;
  statusT1ext: string;

  private userIdSubject = new Subject<string>();

  readonly blogPosts$ = this.userIdSubject.pipe(
    debounceTime(250),
    distinctUntilChanged(),
    switchMap(data => {
      return this.getProjectByName(data);
    })
  );

  wsConnection: boolean = false;
  ot_type: string = 'MARKET';
  product_type: string = 'NRML';
  retention_type: string = 'DAY';
  futureMarkets: any;
  lot_configuration: any = [];

  constructor(inj: Injector, private modalService: ModalService,
    private CONSTANT: ConstantsService,
    private elRef: ElementRef,
    private renderer: Renderer2,
    private serice: AuthService,
    // private socketService: SocketService,
    private shareData: SharedataService,
    private scriptLoadData: ScriptLoaderService) {
    super(inj);
    this.isActive = false;
    this.isActiveCnfm = false;
    this.isLoadFirst = false;
    this.orderOption = CONSTANT.DEFUALT_ORDER_OPTION;
    this.productOption = CONSTANT.DEFUALT_PRODUCT_OPTION;
    this.retentionOption = CONSTANT.DEFUALT_RETENTION_OPTION;

    this.isDestroy = false;
    isweekend = this.isWeekend(this.getTodayDate());
    if (!isweekend) {
      this.initWsConnection();
    } else {
      this.getLiveQuote();
    }

  }

  ngOnInit() {
    // this.getLiveQuote();

    this.initData();
    this.getfuturemarket()
    this.getlotconfiguration()
  }

  getlotconfiguration() {

    this.serice.lotconfiguration().subscribe(
      res => {
        this.lot_configuration = res.data
      },
      error => {
        console.log(error);
      }
    );

  }
  getfuturemarket() {
    this.serice.futureShearMaket().subscribe(
      res => {
        this.futureMarkets = res.data
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    // this.socketService.close();
    // this.socketService.messages.unsubscribe();
  }

  private initWsConnection(): void {
    this.getLiveQuote();
    // this.socketService.messages.subscribe(res => {
    //     // console.log("Response from websocket: ", res);
    //     if (res.length) {
    //       this.wsConnection = true;
    //       this.bindDashboard(res);
    //       const tThis = this;
    //       const clickButtons = this.elRef.nativeElement.querySelectorAll('.order-place');
    //       for (let i = 0; i < clickButtons.length; i++) {
    //         this.renderer.listen(clickButtons[i], 'click', ($event) => {
    //           tThis.openModal('custom-modal-1', clickButtons[i].dataset);
    //         });
    //       }
    //     } else {
    //       this.getLiveQuote();
    //     }
    //   },
    //   (error) => {
    //     console.log('error', error);
    //     this.wsConnection = false;
    //     this.getLiveQuote();
    //     this.socketService.close();
    //     this.initWsConnection();
    //   },
    //   () => {
    //     console.log('complete');
    //     this.socketService.close();
    //   });
  }


  getLiveQuote() {
    this.serice.getLiveShearMarketRate().subscribe(res => {
      if (res.status === 1 || res.status === 'success') {
        this.qList = [];
        this.qListTMP = [];
        if (res.status === 'success') {
          const thiss = this;
          Object.keys(res.data).map(function (data) {
            // return [data, res.data[data]];
            const cData = new Quotes(res.data[data]);
            const depthItem = { 'buy': cData.depth.buy[0], 'sell': cData.depth.sell[0] };
            cData.depth = depthItem;
            cData.tradingSymbol = data;
            cData.low = res.data[data].ohlc.low;
            cData.high = res.data[data].ohlc.high;
            thiss.qList.push(cData);
            thiss.qListTMP.push(cData);
          });
          // this.cd.markForCheck();
        }
        // else {
        // res.data.forEach((item) => {
        //   const cData = new Quotes(item);
        //   cData.low = item.ohlc.low;
        //   cData.high = item.ohlc.high;
        //   // const depthItem = {'buy': JSON.parse(cData.depth).buy[0], 'sell': JSON.parse(cData.depth).sell[0]};
        //   // cData.depth = depthItem;
        //  this.qList.push(cData);
        //   this.qListTMP.push(cData);
        // });
        // }
        const sData = this.arraySorting(res.data);

        this.bindDashboard(sData);
        const tThis = this;
        const clickButtons = this.elRef.nativeElement.querySelectorAll('.order-place');
        for (let i = 0; i < clickButtons.length; i++) {
          this.renderer.listen(clickButtons[i], 'click', ($event) => {
            tThis.openModal('custom-modal-1', clickButtons[i].dataset);
            this.lastPrice.sell = this.q_tmp[i].depth.sell
            this.lastPrice.buy = this.q_tmp[i].depth.buy
          });
        }

        const clickButtons3 = this.elRef.nativeElement.querySelectorAll('.openCollapse');
        for (let i = 0; i < clickButtons3.length; i++) {
          this.renderer.listen(clickButtons3[i], 'click', ($event) => {
            if (this.buySellOpen === clickButtons3[i].dataset.index) {
              this.buySellOpen = -1;
            } else {
              this.buySellOpen = clickButtons3[i].dataset.index;
            }
          });
        }

        const clickButtons1 = this.elRef.nativeElement.querySelectorAll('.order-plc1');
        for (let i = 0; i < clickButtons1.length; i++) {
          this.renderer.listen(clickButtons1[i], 'click', ($event) => {
            tThis.openModal('modal-1', clickButtons1[i].dataset);
          });
        }
        const clickButtons2 = this.elRef.nativeElement.querySelectorAll('.order-plc2');
        for (let i = 0; i < clickButtons2.length; i++) {
          this.renderer.listen(clickButtons2[i], 'click', ($event) => {
            tThis.openModal('modal-2', clickButtons2[i].dataset);
          });
        }
        if (!this.isDestroy && !this.wsConnection && !isweekend) {
          const xhm = setTimeout(() => {
            clearTimeout(xhm);
            this.getLiveQuote();
          }, 1000);
        }
      }
    });
  }

  bindDashboard(res) {
    let html = '';
    if (!this.isLoadFirst) {
      this.q_tmp = res;
      this.isLoadFirst = true;
    }
    if (res.length) {
      this.qListTMP = [];
      res.forEach((item, index) => {
        const cData = new Quotes(item);
        cData.low = item.ohlc.low;
        cData.high = item.ohlc.high;
        const _q_tmp = this.q_tmp[index]
        // debugger;
        // if (cData.name === 'USDINR') {
        //   cData.depth.sell = Number(item.depth.sell).toFixed(2);
        //   cData.depth.buy = Number(item.depth.buy).toFixed(2);
        // }

        this.qListTMP.push(cData);

        let sellColor = '';
        let buyColor = '';
        if (_q_tmp.name === cData.name) {
          if (_q_tmp.depth.sell !== cData.depth.sell) {
            _q_tmp.depth.sell <= cData.depth.sell ? sellColor = 'red-box' : sellColor = 'green-box';
          }
          if (_q_tmp.depth.buy !== cData.depth.buy) {
            _q_tmp.depth.buy <= cData.depth.buy ? buyColor = 'green-box' : buyColor = 'red-box';
          }
        }
        // debugger
        if (this.isOrderIndex === index) { // ORDER RATE CHANGE
          this.orderDetail.buyPrice = cData.depth.buy;
          this.orderDetail.sellPrice = cData.depth.sell;
          // this.sellRate.nativeElement.innerText = cData.depth.sell;
          // this.buyRate.nativeElement.innerText = cData.depth.buy;
        }

        html += '<div class="col-md-6 col-12">';
        // html += '<div id="drop-' + index + '" class="dragable-inner" draggable="true">'
        // html += '<a class="order-place draggable-item" data-modal="custom-modal-1"  data-uindex="' + cData.tradingSymbol + '" data-index="' + index + '">';
        // html += '<a class="order-place" data-modal="custom-modal-1" data-index="' + index + '">';
        html += '<a data-toggle="collapse" class="openCollapse" href="#data-' + index + '" data-index="' + index + '">';
        html += '<div class="bg-card trade-acc">';
        html += '<div class="row align-items-center">';
        html += '<div class="col-md-8 col-5 px-1">';
        html += '<p class="trade-name">' + cData.name;
        html += '<span class="trade-timer">' + cData.expiry + '</span></p></div>';
        html += '<div class="col-md-2 col-3 px-1">';
        html += '<p class="low-high-trade"><b class="red ' + sellColor + '">' + cData.depth.sell + '</b></p>';
        html += '<small class="tht-text">low: ' + cData.low + '</small></div>';
        html += '<div class="col-md-2 col-4 px-1">';
        html += '<p class="low-high-trade m-0"><b class="golden ' + buyColor + '">' + cData.depth.buy + '</b></p>';
        html += '<small class="tht-text">high: ' + cData.high + '</small>';
        html += '</div></div>';

        let collapse1;
        if (this.buySellOpen != index) {
          collapse1 = 'collapse'
        }

        html += '<div id="data-' + index + '" class="' + collapse1 + '">';
        html += '<div class="row col-md-12 col-12 px-2"><div class="col-md-6 col-6 text-center"><a class="btn btn-sell btn-sm btn-outline-danger ml-2 col-sm-5 order-plc1" data-index="' + index + '">Sell</a></div>';
        html += '<div class="col-md-6 col-6 text-center"><a class="btn btn-buy btn-sm btn-outline-warning ml-2 col-sm-5 order-plc2" data-index="' + index + '">Buy</a></div></div>';

        html += '</a></div></div></div>';

        this.q_tmp[index] = cData;
      });
    }
    if (this.isActiveCnfm != true && this.isActive != true) {
      this.quoteRef.nativeElement.innerHTML = html;
    }
    // if (this.isLoadDragJs) {
    // this.isLoadDragJs = false;
    // this.loadeScript();
    // }
  }

  // confirmModel(frmData, trans_type = '') {
  //   const data = frmData;
  //   this.type = trans_type;

  //   if (this.lastPrice.sell === data.sellPrice && this.lastPrice.buy === data.buyPrice) {
  //     this.placeOrder(frmData, trans_type)
  //   } else {
  //     this.modalService.open('custom-modal-3');
  //   }
  // }

  closeAllModal() {
    this.closeModal('custom-modal-3')
    this.closeModal('custom-modal-1')
  }

  openModal(id: string, data?) {

    if (id === 'custom-modal-1') {
      this.orderOption = 'instant_execution';
      this.isOrderIndex = Number(data.index);
      this.initOrder(this.q_tmp[data.index]);
      this.modalService.open(id);
    }

    if (id === 'modal-1') {
      this.isActive = true;
      this.type = "SELL";
      this.orderOption = 'MARKET';
      this.retentionOption = 'DAY';
      this.productOption = 'NRML';
      this.isOrderIndex = Number(data.index);
      this.initOrder(this.q_tmp[data.index]);
    }
    if (id === 'modal-2') {
      this.isActive = true;
      this.type = "BUY";
      this.orderOption = 'MARKET';
      this.retentionOption = 'DAY';
      this.productOption = 'NRML';
      this.isOrderIndex = Number(data.index);
      this.initOrder(this.q_tmp[data.index]);
    }
    if (id === 'custom-modal-2') {
      this.modelClick = data
      this.modalService.open(id);
    }
    this.blogPosts$;
    // this.modalService.open(id);
  }

  closeModal(id: string) {
    this.orderOption = this.CONSTANT.DEFUALT_ORDER_OPTION;
    this.orderDetail = {};
    this.modalService.close(id);
    this.isModalOpen = false;
  }

  modelClosed() {
    this.isActive = false;
    this.buySellOpen = -1;
    if (this.isActiveCnfm === true) {
      this.isActiveCnfm = false;
    }
    this.orderOption = this.CONSTANT.DEFUALT_ORDER_OPTION;
    this.productOption = this.CONSTANT.DEFUALT_PRODUCT_OPTION;
    this.retentionOption = this.CONSTANT.DEFUALT_RETENTION_OPTION;
    this.orderDetail = {};
    this.getLiveQuote();
  }

  confirmModel() {
    this.orderDetail.trans_type = this.type;
    this.isActiveCnfm = true;
  }

  editOrder() {
    this.isActiveCnfm = false;
  }

  initOrder(data) {
    const opn = {
      orderOption: this.orderOption,
      productOption: this.productOption,
      retentionOption: this.retentionOption
    }
    this.orderDetail = this.initOrders(data, opn);
    this.orderDetail.disc_lot = '0';
    if (this.lot_configuration.length > 0) {
      let filter = this.lot_configuration.find(ele => ele.key_name === data.name)

      // if (filter != undefined) {
      //   this.options = { floor: filter.min, ceil: filter.max }
      //   console.log(this.options)
      // }

      if (filter === undefined) {
        this.options = { floor: 0, ceil: 0, unit: undefined, buttonsConfig: [] }
      } else {
        this.options = { floor: filter.min, ceil: filter.max, unit: filter.unit, buttonsConfig: filter.buttonsConfig }
      }
    }

  }

  initData() {
    this.orderDetail = this.resetOrder();
    this.lotArr = this.initLot();
    this.sellOptions = this.initOrderTimeOptions();
    this.orderOptionList = this.initOrderOption();
    this.productTypeOpnList = this.initProductType();
    this.orderTypeOpnList = this.initOrderType();
    this.retentionTypeOpnList = this.initRetentionType();
  }

  searchPosts(userId: string) {
    this.userIdSubject.next(userId);
  }

  iniAddQuote(res) {
    this.qAdd.push(res);
  }

  addTofavorate(id) {
    let data = {
      "instrument_token": id
    }
    this.serice.add_shear_market(data).subscribe(
      res => {
        this.closeModal('custom-modal-2');
        window.location.reload();
      },
      error => {
        console.log(error);
      }
    );
  }
  removeMarket(instrumentToken) {
    if (instrumentToken) {
      let data = {
        "instrument_token": instrumentToken
      }

      swal.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          this.serice.remove_shear_market(data).subscribe(
            res => {
              if (res.status === 1) {
                this.closeModal('custom-modal-2');
                window.location.reload();
                swal.fire(
                  'Deleted!',
                  res.message,
                  'success'
                );
                const tmpTime = setTimeout(() => {
                  swal.close();
                  clearTimeout(tmpTime);
                }, 2000);
              } else {
                swal.fire(
                  'Error!',
                  res.message,
                  'error'
                );
              }
            },
            error => {
              console.log(error);
              swal.close()

            }
          );
        }
      });


    }

  }


  getProjectByName(name: String) {
    const tmp = [];
    const tt = this.qListTMP.filter(proj => proj.name === name);
    tmp.push(tt);
    return tmp;
  }

  addNewQuote() {
    this.closeModal('custom-modal-2');
    this.qAdd.forEach((item) => {
      this.qList.push(item);
    });
  }

  orderOptionChange(e) {
    const action = e;
    this.orderOption = e;
    this.orderDetail.ot_type = e;
    // switch (action) {
    //   case this.CONSTANT.DEFUALT_ORDER_OPTION:
    //     this.orderDetail.deviation = 0;
    //     break;
    //   case this.CONSTANT.BUY_LIMIT:
    //     this.orderDetail.price = '';
    //     this.orderDetail.trans_type = this.CONSTANT.TYPE_BUY;
    //     break;
    //   case this.CONSTANT.SELL_LIMIT:
    //     this.orderDetail.price = '';
    //     this.orderDetail.trans_type = this.CONSTANT.TYPE_SELL;
    //     break;
    // }
  }
  productOptionChange(e) {
    this.productOption = e;
    this.orderDetail.product_type = e;
    if (e === 'BO') {
      this.orderOption = 'LIMIT';
      this.orderDetail.ot_type = 'LIMIT';
      // this.orderDetail.price = '';
      // this.orderDetail.SL = '';
      // this.orderDetail.TP = '';
    }
    if (e === 'CO') {
      this.orderOption = 'MARKET';
      this.orderDetail.ot_type = 'MARKET';
      // this.orderDetail.trigger_price = '';
    }
  }
  retentionOptionChange(e) {
    this.retentionOption = e;
    this.orderDetail.retention_type = e;
  }

  setSellDay(e) {

  }

  increment(str) {
    switch (str) {
      case this.CONSTANT.DEVIATION:
        this.orderDetail[this.CONSTANT.DEVIATION] += 1;
        break;
      case this.CONSTANT.TP:
        if (this.orderDetail[this.CONSTANT.TP] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.TP] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.TP] += 1;
        break;
      case this.CONSTANT.SL:
        if (this.orderDetail[this.CONSTANT.SL] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.SL] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.SL] += 1;
        break;
      case this.CONSTANT.PRICE:
        if (this.orderDetail[this.CONSTANT.PRICE] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.PRICE] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.PRICE] += 1;
        break;
    }
  }

  decrement(str) {
    if (this.orderDetail[str] <= 0) {
      return false;
    }

    switch (str) {
      case this.CONSTANT.DEVIATION:
        this.orderDetail[this.CONSTANT.DEVIATION] -= 1; // (this.orderDetail['deviation'] - 1);
        break;
      case this.CONSTANT.TP:
        if (this.orderDetail[this.CONSTANT.TP] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.TP] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.TP] -= 1;
        break;
      case this.CONSTANT.SL:
        if (this.orderDetail[this.CONSTANT.SL] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.SL] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.SL] -= 1;
        break;
      case this.CONSTANT.PRICE:
        if (this.orderDetail[this.CONSTANT.PRICE] === '') {
          const price = this.orderDetail.orderType === this.CONSTANT.BUY_LIMIT ? this.orderDetail.buyPrice : this.orderDetail.sellPrice;
          this.orderDetail[this.CONSTANT.PRICE] = parseFloat(price);
        }
        this.orderDetail[this.CONSTANT.PRICE] -= 1;
        break;
    }
  }

  placeOrder(frmData, trans_type = '') {
    // frmData.trans_type = this.type;
    // if (trans_type !== '') {
    //   frmData.trans_type = this.type;
    // } 
    // else if (frmData.trans_type === '') {
    //   const trans = frmData.orderType === 'sell_limit' ? 'SELL' : 'BUY';
    //   // trans = frmData.orderType === 'buy_limit' ? 'BUY' : '';
    //   frmData.trans_type = trans;
    // }

    // this.closeModal('custom-modal-1');
    // this.closeModal('custom-modal-3')

    this.modelClosed();

    swal.queue([{
      allowOutsideClick: false,
      allowEscapeKey: false,
      // showConfirmButton: false,
      // showCloseButton: false,
      showCancelButton: false,
      title: this.CONSTANT.ORDER_QUEUE_TITLE_MSG,
      text: this.CONSTANT.ORDER_QUEUE_TEXT_MSG,
      onOpen: () => {
        swal.showLoading();
        this.serice.orderPlace(frmData)
          .subscribe(
            res => {
              swal.hideLoading();
              const type = res.status === 1 ? 'success' : 'error';
              swal.update({
                icon: type,
                title: res.message,
                text: res.data !== null && res.data !== undefined ? res.data : '',
              });
              this.shareData.sendCall(1); // CALL BALANCE CHANGE
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, 2000);
            },
            error1 => {
              swal.hideLoading();
              swal.update({
                icon: 'error',
                title: this.CONSTANT.ORDER_REQ_ERROR_TITLE,
                text: this.CONSTANT.ORDER_REQ_ERROR_TEXT,
                showConfirmButton: true,
                showCloseButton: true,
                timer: 1500
              });
              const tmpTime = setTimeout(() => {
                swal.close();
                clearTimeout(tmpTime);
              }, 1500);
            });
      }
    }]);
  }

  // loadeScript() {
  //   this.scriptLoadData.loadScript('script', 'assets/js/drag.js', false);
  // }

}
