import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  template: `
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item">
          <a class="page-link"   (click)="firstPag()">|<</a>
        </li>
        <li class="page-item">
          <a class="page-link"   (click)="prevPage()"><</a>
        </li>
        <li class="page-item">
          <a class="page-link"   (click)="nextPage()">></a>
        </li>
        <li class="page-item">
          <a class="page-link"   (click)="lastPag()">>|</a>
        </li>
        <li class="page-item disabled">
          <span class="page-link">Total recode: {{ totalPages }}</span>
        </li>
      </ul>
    </nav>`
})
export class PaginationComponent implements OnInit {
  @Input('page') page: number;
  @Input('total') total: number;
  max_results: number = 5;
  totalPages: number;
  qtdPagination = 5;
  arrayPages: number[];
  @Output() modulePagging = new EventEmitter();
  linkNull = 'javascript:void(0)';

  constructor() {
  }

  ngOnInit() {
  }

  listaPagination() {
    this.arrayPages = [];
    for (let i = 1; i <= this.totalPages; i++) {
      this.arrayPages.push(i);
    }

  }

  @Input('totalPages')
  set updateTotalPagesValue(totalPages: number) {
    this.totalPages = totalPages;
    this.listaPagination();
  }

  mainPage(pag: number) {
    console.log(pag);
    this.modulePagging.emit({ valor: pag });
  }

  nextPage() {
    if (this.page < this.total) {
      this.mainPage(this.page + 1);
    }
  }

  prevPage() {
    if (this.page > 1) {
      this.mainPage(this.page - 1);
    }
  }

  firstPag() {
    this.mainPage(1);
  }

  lastPag() {
    const metadePgs = Math.ceil(this.totalPages / this.total);

    this.mainPage(metadePgs);
  }

  diminuiPaginacao(pags) {
    const metadePgs = Math.ceil(this.totalPages / this.max_results);
    this.total = metadePgs;
    let i = this.page - metadePgs;
    if (i < 0) {
      i = 0;
    }
    let f = i + metadePgs;
    if (f > pags.length) {
      f = pags.length;
      i = f - metadePgs;
      if (i < 0) {
        i = 0;
      }
    }
    return pags.slice(i, f);
  }
}
