import {Component, ElementRef, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';

import {ModalService} from '../../core/services/modal.service';

@Component({
  selector: 'jw-modal',
  template:
      `
    <div class="modal fade" #mRef id="SellModal" tabindex="-1" role="dialog" aria-labelledby="SellModal" aria-hidden="true">
      <div class="modal-dialog buy-sell-div {{classAdd}}" role="document">
        <ng-content></ng-content>
      </div>
    </div>
    <div class="modal-backdrop fade" #mbRef></div>`
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input() id: string;
  private element: any;
  public classAdd: string;
  @ViewChild('mRef') mRef: ElementRef;
  @ViewChild('mbRef') mbRef: ElementRef;

  constructor(private modalService: ModalService, private el: ElementRef) {
    this.element = el.nativeElement;
    this.classAdd = '';
  }

  ngOnInit(): void {
    let modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    // move element to bottom of page (just before </body>) so it can be displayed above everything else
    document.body.appendChild(this.element);

    // close modal on background click
    this.element.addEventListener('click', function (e: any) {
      if (e.target.className === 'jw-modal') {
        modal.close();
        // this.close();
      }
    });

    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
    this.mbRef.nativeElement.style.display = 'none';
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(className: string = ''): void {
    this.classAdd = className;
    this.mbRef.nativeElement.style.display = 'block';
    this.mbRef.nativeElement.classList.add('show');
    this.mRef.nativeElement.style.display = 'block';
    this.mRef.nativeElement.classList.add('show');
    this.element.style.display = 'block';
    document.body.classList.add('modal-open');
  }

  // close modal
  close(): void {
    this.mRef.nativeElement.style.display = 'none';
    this.mRef.nativeElement.classList.remove('show');
    this.element.style.display = 'none';
    document.body.classList.remove('modal-open');
  }
}
