import { Component, OnInit, PLATFORM_ID, Injector, NgZone, APP_ID } from '@angular/core';
import { TransferState, makeStateKey, Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as moment from 'moment';
import swal from 'sweetalert2';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'parent-comp',
  template: ``,
  providers: []
})

export class BaseComponent {

  public activatedRoute: ActivatedRoute;
  public routeUrl: any;
  public titleService: Title;
  public metaService: Meta;
  public platformId: any;
  public appId: any;
  public router: Router;
  public baseUrl;

  constructor(injector: Injector) {
    this.router = injector.get(Router);
    this.platformId = injector.get(PLATFORM_ID);
    this.appId = injector.get(APP_ID);
    this.titleService = injector.get(Title);
    this.metaService = injector.get(Meta);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
      }
    });
  }

  // *************************************************************//
  // @Purpose : To check server or browser
  // *************************************************************//
  isBrowser() {
    if (isPlatformBrowser(this.platformId)) {
      return true;
    } else {
      return false;
    }
  }

  // *************************************************************//
  // @Purpose : We can use following function to use localstorage
  // *************************************************************//
  setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.setItem(key, value);
      sessionStorage.setItem(key, value);
    }
  }

  getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      // return window.localStorage.getItem(key);
      return sessionStorage.getItem(key);
    }
  }

  removeToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.removeItem(key);
      sessionStorage.removeItem(key);
    }
  }

  clearToken() {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.removeItem('currentUser');localStorage.removeItem('event');;
      sessionStorage.clear();
    }
  }

  // *************************************************************//

  // *************************************************************//
  // @Purpose : We can use following function to use Toaster Service.
  // *************************************************************//
  popToast(type, title) {
    swal.fire({
      position: 'center',
      // type: type,
      text: title,
      showConfirmButton: false,
      timer: 3000,
      // customClass: 'custom-toaster'
    });
  }

  /****************************************************************************
   @PURPOSE      : To restrict or allow some values in input.
   @PARAMETERS   : $event
   @RETURN       : Boolen
   ****************************************************************************/
  RestrictSpace(e) {
    if (e.keyCode == 32) {
      return false;
    } else {
      return true;
    }
  }

  AllowNumbers(e) {
    var input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 43 || e.which === 45) {
      return true;
    }
    if (e.which === 36 || e.which === 35) {
      return true;
    }
    if (e.which === 37 || e.which === 39) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  /****************************************************************************/
  getProfile() {
    const url = this.getToken('ss_pic');
    if (url == null || url === ' ') {
      return 'assets/images/NoProfile.png';
    } else {
      return url;
    }
  }

  /****************************************************************************
   //For COOKIE
   /****************************************************************************/
  setCookie(name, value, days) {
    var expires = '';
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
  }

  getCookie(name) {
    var nameEQ = name + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) == 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }

  eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
  }

  /****************************************************************************
   //For Side menu toggle
   /****************************************************************************/
  slideLeft() {
    $('body').addClass('slide-open');
  }

  removeSlide() {
    $('body').removeClass('slide-open');
  }

  slideClose() {
    $('body').removeClass('slide-open');
  }

  /****************************************************************************/
  convertTimestamp(timeStamp) {
    // debugger
    return moment(timeStamp).format('lll');
  }

  getTodayDate() {
    return moment().format('YYYY-MM-DD');
  }

  /****************************************************************************
   //For SLUG TO TITLE CONVERT
   /****************************************************************************/
  generateTitle(slug: string) {
    const words = slug.split('-');
    for (let i = 0; i < words.length; i++) {
      let word = words[i];
      words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }
    return words.join(' ');
  }

  arraySorting(data) {
    // console.log(data);
    return data;
    // return data.sort((a, b) => (a.name > b.name) ? 1 : -1);
  }

  /****************************************************************************
   //For REMOVE BODY CLASS
   /****************************************************************************/
  removeBodyClass() {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    body.classList.remove('box-collapse-open');
    body.classList.remove('box-collapse-close');
  }

  isWeekend(dateString) {
    /* Step 1 */
    const dateArray = dateString.split('-');
    /* Step 2 */
    const date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
    /* Step 3 */
    const day = date.getDay();
    /* Step 4 */
    return day === 0 || day === 6;
  }

  sortingArray(data: any, key: string, option: string = 'asc1') {
    if (data.length) {
      switch (key) {
        case 'order':
          return option === 'asc' ? data.sort() : data.sort((a, b) => b.name - a.name);
          break;
        case 'symbol':
          return data.sort(function (a, b) {
            return a.tradingSymbol.localeCompare(b.tradingSymbol);
          });
          break;
        case 'profit':
          return data.sort(function (a, b) {
            return a.profit - b.profit;
          });
          break;
        case 'time':
          return data.sort(function (a, b) {
            return <any>new Date(b.created_at) - <any>new Date(a.created_at);
          });
          break;
        case 'open_time':
          return data.sort(function (a, b) {
            return <any>new Date(b.created_at) - <any>new Date(a.created_at);
          });
          break;
        case 'close_time':
          return data.sort(function (a, b) {
            return <any>new Date(b.updated_at) - <any>new Date(a.updated_at);
          });
          break;
        default:
          throw new Error('Type of T is not a valid return type!');
      }
    } else {
      // throw new Error('Key \'' + key + '\' does not exist!');
      return [];
    }
  }

  initOrders(data, opn ,depth: any = '') {
    let sellprice = '';
    let buyprice = '';
    if (depth === '') {
      sellprice = data.depth.sell.price ? data.depth.sell.price : data.depth.sell;
      buyprice = data.depth.buy.price ? data.depth.buy.price : data.depth.buy;
    }
    if (depth) {
      sellprice = depth[data.name].sell;
      buyprice = depth[data.name].buy;
    }

    if (buyprice && sellprice) {

      const orderDetail = {
        name: data.name,
        tradingSymbol: data.tradingSymbol,
        instrumentToken: data.instrument_token ? data.instrument_token : data.instrumentToken,
        expiry: data.expiry ? data.expiry : data.last_trade_time,
        orderType:'',
        product_type: opn.productOption,
        retention_type: opn.retentionOption,
        ot_type: opn.orderOption,
        disc_lot: '',
        lot: '',
        buyPrice: buyprice,
        sellPrice: sellprice,
        SL: '',
        TP: '',
        deviation: 0,
        price: '',
        trans_type: ''
      };
      return orderDetail;
    } else {
      return null;
    }
  }

  resetOrder() {
    const tmp = {
      name: '',
      tradingSymbol: '',
      instrumentToken: '',
      expiry: '',
      product_type: '',
      retention_type: '',
      ot_type: '',
      disc_lot: '',
      lot: '',
      buyPrice: '',
      sellPrice: '',
      SL: '',
      TP: '',
      deviation: 0,
      price: '',
      trans_type: ''
    };
    return tmp;
  }

  initLot() {
    const lotArr = {};
    const gLot = [
      {
        'name': '100gm',
        'value': '100gm'
      },
      {
        'name': '1kg',
        'value': '1kg'
      },
      {
        'name': '3kg',
        'value': '3kg'
      }];
    const slLot = [
      {
        'name': '1kg',
        'value': '1kg'
      },
      {
        'name': '5kg',
        'value': '5kg'
      },
      {
        'name': '30kg',
        'value': '30kg'
      }];
    const uiLot = [
      {
        'name': '1k',
        'value': '1k'
      },
      {
        'name': '10k',
        'value': '10k'
      },
      {
        'name': '50k',
        'value': '50k'
      }];
    lotArr['GOLD'] = gLot;
    lotArr['SILVER'] = slLot;
    lotArr['USDINR'] = uiLot;
    return lotArr;
  }

  initOrderOption() {
    const options = [
      {
        'name': 'instant execution',
        'value': 'instant_execution'
      },
      {
        'name': 'buy limit',
        'value': 'buy_limit'
      },
      {
        'name': 'sell limit',
        'value': 'sell_limit'
      }
      // ,
      // {
      //   'name': 'buy stop',
      //   'value': 'buy_stop'
      // },
      // {
      //   'name': 'sell stop',
      //   'value': 'sell_stop'
      // },
      // {
      //   'name': 'buy stop limit',
      //   'value': 'buy_stop_limit'
      // },
      // {
      //   'name': 'sell stop limit',
      //   'value': 'sell_stop_limit'
      // }
    ];
    return options;
  }

  initOrderTimeOptions() {
    const options2 = [
      {
        'name': 'GTC',
        'value': 'gtc'
      },
      {
        'name': 'Today',
        'value': 'today'
      },
      {
        'name': 'Specified',
        'value': 'specified'
      },
      {
        'name': 'Specified Day',
        'value': 'specified_day'
      }
    ];
    return options2;
  }

  initRetentionType() {
    const options = [
      {
        'name': 'DAY',
        'value': 'DAY'
      },
      {
        'name': 'IOC',
        'value': 'IOC'
      },
      {
        'name': 'EOS',
        'value': 'EOS'
      },
      {
        'name': 'GTD',
        'value': 'GTD'
      },
    ];
    return options;
  }
  initOrderType() {
    const options = [
      {
        'name': 'MARKET',
        'value': 'MARKET'
      },
      {
        'name': 'LIMIT',
        'value': 'LIMIT'
      },
      {
        'name': 'SL',
        'value': 'SL'
      },
      {
        'name': 'SL-M',
        'value': 'SLM'
      },
    ];
    return options;
  }

  initProductType() {
    const options = [
      {
        'name': 'NRML',
        'value': 'NRML'
      },
      {
        'name': 'MIS',
        'value': 'MIS'
      },
      {
        'name': 'CO',
        'value': 'CO'
      },
      {
        'name': 'BO',
        'value': 'BO'
      },
    ];
    return options;
  }

  lotCal(key: any, type: string) {
    return key
    if (typeof key == 'number') { return key }
    if (type === 'GOLD') {
      switch (key) {
        case '100g':
          return 10;
          break;
        case '100gm':
          return 10;
          break;
        case '1kg':
          return 100;
          break;
        case '3kg':
          return 300;
          break;
      }
    }

    if (type === 'SILVER') {
      switch (key) {
        case '1kg':
          return 1;
          break;
        case '5kg':
          return 5;
          break;
        case '30kg':
          return 30;
          break;
      }
    }

    if (type === 'USDINR') {
      switch (key) {
        case '1k':
          return 1000;
          break;
        case '10k':
          return 10000;
          break;
        case '50k':
          return 50000;
          break;
      }
    }
    return null;
  }
}
