export interface History {
  id: number;
  uid: number;
  name: string;
  price: number;
  buy_price: number;
  sell_price: number;
  instrumentToken: number;
  tradingSymbol: string;
  stopLoss: number;
  takeProfit: number;
  deviation: number;
  lot: string;
  order_type: string;
  trans_type: string;
  profit: number;
  closeBuyPrice: number;
  closeSellPrice: number;
  comm: number;
  margin: number;
  expiry: any;
  status: number;
  created_at: any;
  updated_at: any;
  ot_type?: string;
  product_type?: string;
  retention_type?: string;
  disc_lot?: number;
  trigger_price: number;
}

export class HistoryObj implements History {

  id: number;
  uid: number;
  name: string;
  price: number;
  buy_price: number;
  sell_price: number;
  instrumentToken: number;
  tradingSymbol: string;
  stopLoss: number;
  takeProfit: number;
  deviation: number;
  lot: string;
  order_type: string;
  trans_type: string;
  profit: number;
  closeBuyPrice: number;
  closeSellPrice: number;
  comm: number;
  margin: number;
  expiry: any;
  status: number;
  created_at: any;
  updated_at: any;
  ot_type?: string;
  product_type?: string;
  retention_type?: string;
  disc_lot?: number;
  trigger_price:number;

  constructor(item?: History) {
    this.id = item.id;
    this.uid = item.uid;
    this.name = item.name;
    this.price = item.price;
    this.buy_price = item.buy_price;
    this.sell_price = item.sell_price;
    this.instrumentToken = item.instrumentToken;
    this.tradingSymbol = item.tradingSymbol;
    this.stopLoss = item.stopLoss;
    this.takeProfit = item.takeProfit;
    this.deviation = item.deviation;
    this.lot = item.lot;
    this.order_type = item.order_type;
    this.trans_type = item.trans_type;
    this.profit = item.profit;
    this.closeBuyPrice = item.closeBuyPrice;
    this.closeSellPrice = item.closeSellPrice;
    this.comm = item.comm;
    this.margin = item.margin;
    this.expiry = item.expiry;
    this.status = item.status;
    this.created_at = item.created_at;
    this.updated_at = item.updated_at;
    this.ot_type = item.ot_type;
    this.product_type = item.product_type;
    this.retention_type = item.retention_type;
    this.disc_lot = item.disc_lot;
    this.trigger_price =item.trigger_price;
  }
}
