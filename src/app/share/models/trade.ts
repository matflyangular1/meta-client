export interface TradeList {
  name: string;
  tradingSymbol: string;
  instrumentToken: number;
  buy_price: number;
  sell_price: number;
  lot?: any;
  order_type?: string;
  trans_type?: string;
  takeProfit?: number;
  stopLoss?: number;
  profit?: any;
  price: number;
  id: number;
  status: number;
  created_at: string;
  updated_at: string;
  is_update: number;
  ot_type?: string;
  product_type?: string;
  retention_type?: string;
  disc_lot?: string;
  trigger_price: number;
}

export class Trade implements TradeList {
  name: string;
  tradingSymbol: string;
  instrumentToken: number;
  buy_price: number;
  sell_price: number;
  lot?: any;
  order_type?: string;
  trans_type?: string;
  takeProfit?: number;
  stopLoss?: number;
  profit?: any;
  price: number;
  id: number;
  created_at: string;
  updated_at: string;
  status: number;
  is_update: number;
  ot_type?: string;
  product_type?: string;
  retention_type?: string;
  disc_lot?: string;
  trigger_price: number;
  
  constructor(item?: TradeList) {
    this.name = item.name;
    this.instrumentToken = item.instrumentToken;
    this.tradingSymbol = item.tradingSymbol;
    this.buy_price = item.buy_price;
    this.sell_price = item.sell_price;
    this.lot = item.lot;
    this.order_type = item.order_type;
    this.trans_type = item.trans_type;
    this.takeProfit = item.takeProfit === null ? 0 : item.takeProfit;
    this.stopLoss = item.stopLoss === null ? 0 : item.stopLoss;
    this.profit = item.profit;
    this.price = item.price;
    this.id = item.id;
    this.created_at = item.created_at;
    this.updated_at = item.updated_at;
    this.status = item.status;
    this.is_update = -1;
    this.ot_type = item.ot_type;
    this.product_type = item.product_type;
    this.retention_type = item.retention_type;
    this.disc_lot = item.disc_lot;
    this.trigger_price =item.trigger_price;
  }
}
