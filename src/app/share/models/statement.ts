export interface Statements {
    amount: number;
    balance: number;
    buyPrice: number;
    closeBuyPrice: number;
    closeSellPrice: number;
    stopLoss: number;
    takeProfit: number;
    created_on: any;
    eType: number;
    id: number;
    mType: string;
    name: string;
    order_type: string;
    sellPrice: number;
    status: number;
    tradingSymbol: string;
    trans_type: string;
    type: string;
    updated_on: any;
    description: any;
    commission: number;
    completeOrderId: string
}

export class StatementsObj implements Statements {
    amount: number;
    balance: number;
    buyPrice: number;
    closeBuyPrice: number;
    closeSellPrice: number;
    stopLoss: number;
    takeProfit: number;
    created_on: any;
    eType: number;
    id: number;
    mType: string;
    name: string;
    order_type: string;
    sellPrice: number;
    status: number;
    tradingSymbol: string;
    trans_type: string;
    type: string;
    updated_on: any;
    description: any;
    commission: number;
    completeOrderId: string


    constructor(item?: Statements) {
        this.amount = item.amount ? item.amount : item.commission;
        this.balance = item.balance;
        this.buyPrice = item.buyPrice;
        this.closeBuyPrice = item.closeBuyPrice;
        this.closeSellPrice = item.closeSellPrice;
        this.stopLoss = item.stopLoss;
        this.takeProfit = item.takeProfit;
        this.created_on = item.created_on;
        this.eType = item.eType;
        this.id = item.id;
        this.mType = item.mType;
        this.name = item.name;
        this.order_type = item.order_type;
        this.sellPrice = item.sellPrice;
        this.status = item.status;
        this.tradingSymbol = item.tradingSymbol;
        this.trans_type = item.trans_type;
        this.type = item.type;
        this.updated_on = item.updated_on;
        this.description = item.description;
        this.commission = item.commission;
        this.completeOrderId = item.completeOrderId
    }
}