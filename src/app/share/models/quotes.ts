export interface QuotesList1 {
  name: string;
  tradingSymbol: string;
  instrumentToken: number;
  lastPrice: number;
  high: number;
  low?: number;
  open?: number;
  close?: number;
  expiry?: any;
}

export class Quotes1 implements QuotesList1 {
  name: string;
  tradingSymbol: string;
  instrumentToken: number;
  lastPrice: number;
  high: number;
  low?: number;
  open?: number;
  close?: number;
  expiry?: any;

  constructor(item?: Quotes1) {
    this.name = item.name;
    this.instrumentToken = item.instrumentToken;
    this.tradingSymbol = item.tradingSymbol;
    this.lastPrice = item.lastPrice;
    this.high = item.high;
    this.low = item.low;
    this.open = item.open;
    this.close = item.close;
    this.expiry = item.expiry;
  }
}


export interface QuotesList {
  name: string;
  instrument_token: number;
  tradingSymbol: string;
  timestamp: any;
  last_trade_time: any;
  last_price: number;
  last_quantity: number;
  buy_quantity: number;
  sell_quantity: number;
  volume: number;
  average_price: number;
  oi: number;
  oi_day_high: number;
  oi_day_low: number;
  net_change: number;
  lower_circuit_limit: number;
  upper_circuit_limit: number;
  high: number;
  low: number;
  depth: any;
  expiry: string;
}

export class Quotes implements QuotesList {
  name: string;
  instrument_token: number;
  tradingSymbol: string;
  timestamp: any;
  last_trade_time: any;
  last_price: number;
  last_quantity: number;
  buy_quantity: number;
  sell_quantity: number;
  volume: number;
  average_price: number;
  oi: number;
  oi_day_high: number;
  oi_day_low: number;
  net_change: number;
  lower_circuit_limit: number;
  upper_circuit_limit: number;
  high: number;
  low: number;
  depth: any;
  expiry: string;

  constructor(item?: QuotesList) {
    this.name = item.name;
    this.instrument_token = item.instrument_token;
    this.tradingSymbol = item.tradingSymbol;
    this.timestamp = item.timestamp;
    this.last_trade_time = item.last_trade_time;
    this.last_price = item.last_price;
    this.last_quantity = item.last_quantity;
    this.buy_quantity = item.buy_quantity;
    this.sell_quantity = item.sell_quantity;
    this.volume = item.volume;
    this.average_price = item.average_price;
    this.oi = item.oi;
    this.oi_day_high = item.oi_day_high;
    this.oi_day_low = item.oi_day_low;
    this.net_change = item.net_change;
    this.lower_circuit_limit = item.lower_circuit_limit;
    this.upper_circuit_limit = item.upper_circuit_limit;
    this.high = item.high;
    this.low = item.low;
    this.depth = item.depth;
    this.expiry = item.expiry;
  }
}
