import { Component, OnInit , AfterViewInit, HostListener, Renderer2} from '@angular/core';
import { AuthService, SharedataService } from './core/services/index';

declare var window, InstallTrigger;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Metagold Trader';
  public isDevActive: boolean = false;
  public isDev: any = 'dev';   // This only executes when isDev is 'prod'.
  private render: Renderer2;

  constructor(private service: AuthService, private share: SharedataService) {
  }

  ngOnInit() {
    this.checkSiteSetting();
  }

  checkSiteSetting() {
    this.service.getSettings().subscribe((res) => {
      if (res.status === 1) {
        // const tmp = [];
        res.data.forEach((item) => {
          // tmp[item.key_name] = item.value;
          this.share.setSettingOption(item.key_name, item.value);
        });
      } else {
        this.share.setSettingOption('settings', null);
      }
    });
  }

  // Dev Tool Hiding

  ngAfterViewInit() {
    const isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox) {
      this.nwDevTools();
      this.devBlockScript();
    } else {
      this.testDev();
    }
  }

  fnT(e1, t1) {
    window.devtools = { isOpen: e1, orientation: t1 };
    window.dispatchEvent(new CustomEvent('devtoolschange', { detail: { isOpen: e1, orientation: t1 } }));
  }

  nwDevTools() {
    const e: any = { isOpen: !1, orientation: void 0 };
    setInterval(() => {
      const n = window.outerWidth - window.innerWidth > 160, i = window.outerHeight - window.innerHeight > 160,
        r = n ? 'vertical' : 'horizontal';
      i && n || !(window.Firebug && window.Firebug.chrome && window.Firebug.chrome.isInitialized || n || i) ? (e.isOpen && this.fnT(!1, void 0), e.isOpen = !1, e.orientation = void 0) : (e.isOpen && e.orientation === r || this.fnT(!0, r), e.isOpen = !0, e.orientation = r);

    }, 500);
    window.devtools = e;
  }

  testDev() {
    const element: any = new Image();
    this.isDevActive = false;
    element.__defineGetter__('id', () => {
      this.isDevActive = true; // This only executes when devtools is open.
    });
    console.log(element);
    const xhm = setInterval(() => {
      this.isDevActive = false;
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
      if(this.isDev === 'prod') {
        console.log(element);
      }
      // console.clear();
    }, 1e3);
  }

  devBlockScript() {
    document.oncontextmenu = function (e) {
      return !1;
    };
    let f = new Image;
    Object.defineProperty(f, 'id', {
      get: function () {

        this.isDevActive = true;
      }
    }), console.log(f),
      setInterval((function () {
        this.isDevActive = false;
        console.log(f);
      }), 1e3);

    // window.addEventListener('devtoolschange', (function (e) {
    //     this.isDev = e.detail.isOpen;
    //     localStorage.setItem('opendev', this.isDev);
    // }));

    this.render.listen('window', 'devtoolschange', (event) => {
      this.isDevActive = event.detail.isOpen;
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
      if (!event.detail.isOpen) {
        //  window.location.reload();
        setTimeout(() => {
          if (window.innerWidth < 600) {
            this.isDevActive = true;
          } else {
            this.isDevActive = window.devtools.isOpen;
          }
          localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
        }, 1000);
      }
      console.log(this.isDevActive);
    });
    setInterval(() => {
      if (window.innerWidth < 600) {
        this.isDevActive = true;
      } else {
        this.isDevActive = window.devtools.isOpen;
      }
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));

    }, 1000);
  }






}
