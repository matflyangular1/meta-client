import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {WebsocketService} from './websocket.service';

// const CHAT_URL = 'ws://18.216.100.103:6162/';
const CHAT_URL = 'wss://18.216.100.103:3000/';

export interface Message {
  author: string,
  message: string
}

@Injectable()
export class SocketService {
  public messages: Subject<any>;
  public completed: Subject<any>;

  constructor(wsService: WebsocketService) {
    this.messages = <Subject<any>>wsService
      .connect(CHAT_URL)
      .pipe(map((response: MessageEvent): any => {
        let data: any = [];
        if (response.data !== 'HELLO USER!' && response.data !== 'User enter') {
          const rData = JSON.parse(response.data);
          let reDataArr = [];
          Object.keys(rData).map(function (data) {
            const cData = rData[data];
            cData['depth'] = {'buy': cData.depth.buy[0].price, 'sell': cData.depth.sell[0].price};
            // if (cData.name === 'USDINR') {
            //   cData['depth'] = {'buy': cData.depth.buy.toFixed(2), 'sell': cData.depth.sell.toFixed(2)};
            // }
            cData['tradingSymbol'] = data;
            reDataArr.push(cData);
          });
          return reDataArr;
        }
        return data;
      },));

  }

  reconnect() {
    this.messages.next('dd');
  }

  close() {
    this.messages.next('close');
  }
}
