import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  readonly BASE_APP_URL: string = 'http://localhost:10001/';
  readonly DIST_LOCATION: string = 'dist/marketDesktop/';
  readonly DATE_FORMAT: string = 'YYYY-MM-DD';
  readonly SET_INPUT_DATE_TIMEOUT: number = 500;
  readonly PER_PAGE_MAX_RECODE: number = 5;
  readonly DEFUALT_ORDER_OPTION: string = 'MARKET';
  readonly DEFUALT_PRODUCT_OPTION: string = 'NRML';
  readonly DEFUALT_RETENTION_OPTION: string = 'DAY';

  readonly ORDER_QUEUE_TITLE_MSG: string = 'Order has been placed in queue';
  readonly UPDATE_SL_TP_TITLE_MSG: string = 'Order update is in process...';

  readonly ORDER_QUEUE_TEXT_MSG: string = 'Your order confirmation ....';
  readonly ORDER_REQ_ERROR_TITLE: string = 'Order request error!';
  readonly ORDER_REQ_ERROR_TEXT: string = 'Something wrong..., Please try again.';
  readonly SWAL_CLOSE_CLEAR_TIMEOUT: number = 2000;
  readonly SWAL_UPDATE_TIMER: number = 1500;
  readonly TYPE_BUY: string = 'BUY';
  readonly TYPE_SELL: string = 'SELL';
  readonly INSTANT_EXECUTION: string = 'instant_execution';
  readonly BUY_LIMIT: string = 'buy_limit';
  readonly SELL_LIMIT: string = 'sell_limit';
  readonly DEVIATION: string = 'deviation';
  readonly SL: string = 'SL';
  readonly TP: string = 'TP';
  readonly PRICE: string = 'price';
  readonly SILVER: string = 'SILVER';
  readonly SILVER_1KG: string = '1kg';
  readonly HISTORY_TAB_POSITION: string = 'position';
  readonly HISTORY_TAB_ORDER: string = 'order';
  readonly HISTORY_TAB_DEAL: string = 'deal';
  readonly CANCELED: string = 'Canceled';
  readonly FILLED: string = 'filled';

  constructor() { }
}
