import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EnvService {
  API_URL = environment.API_URL;
  API_LIVE_URL = environment.API_LIVE_URL;
  constructor() {

  }
}
