import { HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpEventType, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap, retry, shareReplay, map, catchError } from 'rxjs/operators';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { EnvService } from './env.service';
import { User } from '../../share/models/user';


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  isLoggedIn = false;
  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(data) {
    return this.http.post(this.env.API_URL + 'login', data).pipe(
      tap((token: any) => {
        if (token.status === 1) {
          localStorage.setItem('currentUser', JSON.stringify(token.data));
          localStorage.setItem('isBannershow', '0');
          this.token = token.data;
          this.isLoggedIn = true;
          this.currentUserSubject.next(this.token);
        }
        return token;
      }),
    );
  }

  add_market(data) {
    return this.http.post(this.env.API_URL + 'add-market', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  remove_market(data) {
    return this.http.post(this.env.API_URL + 'remove-market', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  add_shear_market(data) {
    return this.http.post(this.env.API_URL + 'add-share-market', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  remove_shear_market(data) {
    return this.http.post(this.env.API_URL + 'remove-share-market', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  changePassword(data) {
    return this.http.post(this.env.API_URL + 'change-password', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  demoAccount(data) {
    return this.http.post(this.env.API_URL + 'demo-client', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  logout() {
    return this.http.get(this.env.API_URL + 'logout')
      .pipe(
        tap(data => {
          localStorage.removeItem('currentUser');localStorage.removeItem('event');
          sessionStorage.clear();
          this.currentUserSubject.next(null);
          this. isLoggedIn = false;
          delete this.token;
          return data;
        })
      );
  }

  accessDenide() {
    localStorage.removeItem('currentUser');localStorage.removeItem('event');
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    // this.route.navigate(['/auth']);
  }

  headerCall() {
    // const headers = new HttpHeaders({
    //     'Authorization': 'Bearer ' + this.token['token'],
    //     'content-type': 'application/json'
    // });

    return this.http.post<any>(this.env.API_URL + 'user-data', {})
      .pipe(
        retry(4),
        tap(user => {
          // localStorage.setItem('userData', JSON.stringify({
          //   username: user.data.userName,
          //   balance: user.data.balance.balance,
          //   expose: user.data.balance.expose
          // }));
          // localStorage.setItem('betOption', JSON.stringify({betOption: user.data.betOption}));
          // if (user.status === 0 && user.code === 444) {
          //   this.accessDenied();
          // }
          if ([444, 403].indexOf(user.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return user;
        }, err => {
          if (err.status === 0 && err.code === 444) {
            this.accessDenide();
          }
        }),
        shareReplay()
      );
  }

  orderPlace(data) {
    return this.http.post<any>(this.env.API_URL + 'place-order', data) // , {headers: headers}
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  getTradePosition() {
    return this.http.get<any>(this.env.API_URL + 'trade-history') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getPositionOrder(data, page) {
    return this.http.post<any>(this.env.API_URL + `position/order?page=${page}`, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getCurrentOrder(data, page) {
    return this.http.post<any>(this.env.API_URL + `current/order?page=${page}`, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  futureMaket() {
    return this.http.get<any>(this.env.API_URL + 'future-market') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  futureShearMaket() {
    return this.http.get<any>(this.env.API_URL + 'future-share-market') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }


  lotconfiguration() {
    return this.http.get<any>(this.env.API_URL + 'lots') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }
  getLiveMarket() {
    return this.http.get<any>('http://printo-env-1.prib5q8tuu.ap-south-1.elasticbeanstalk.com/commodities') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getLiveMarket1() {
    return this.http.get<any>(this.env.API_URL + 'get-market') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getLiveMarketRate() {
    return this.http.get<any>(this.env.API_URL + 'current-market') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getLiveShearMarketRate() {
    return this.http.get<any>(this.env.API_URL + 'current-share-market') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getLiveMarketDepth() {
    return this.http.get<any>(this.env.API_LIVE_URL + 'sysmarket') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getTradeDetail() {
    return this.http.get<any>(this.env.API_URL + 'get-margin')
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  orderProcessing(data, trans) {
    const endPoint = trans === '' ? 'order-processing' : 'place-order';
    return this.http.post<any>(this.env.API_URL + endPoint, data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  update_sl_tp(data) {
    const endPoint = 'update-sltp';
    return this.http.post<any>(this.env.API_URL + endPoint, data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  update_order(data){
    return this.http.post<any>(this.env.API_URL + 'edit_order', data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        })
      );
  }

  getHistory(data) {
    return this.http.post<any>(this.env.API_URL + 'new-order-history', data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getHistoryType(type, data) {
    return this.http.post<any>(this.env.API_URL + 'new-order-history/' + type, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getStatements(data, page) {
    return this.http.post<any>(this.env.API_URL + `commission-statement?page=${page}`, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getOrderBook(data) {
    return this.http.post<any>(this.env.API_URL + `order-book`, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }


  cancleOrder(data) {
    return this.http.post<any>(this.env.API_URL + `cancel-order`, data)
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getComplain(page) {
    return this.http.get<any>(this.env.API_URL + `complain/list?page=${page}`)
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }
 

  // end Complain File 
  createComplain(data) {
    return this.http.post<any>(this.env.API_URL + `complain/create`, data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            this.accessDenide();
          }
          return res;
        }),
        shareReplay()
      );
  }

  getNewsList() {
    return this.http.get<any>(this.env.API_URL + 'news-list')
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  getSettings() {
    return this.http.get<any>(this.env.API_URL + 'setting')
      .pipe(
        retry(4),
        tap(res => {
          return res;
        }),
        shareReplay()
      );
  }

  deleteOrder(data) {
    return this.http.get<any>(this.env.API_URL + 'delete-order?position=' + data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
          }
          return res;
        })
      );
  }

  getPrintStatement(data) {
    return this.http.post<any>(this.env.API_URL + `statement/list`, data)
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            this.accessDenide();
          }
          return res;
        }),
        shareReplay()
      );
  }

  getToken() {
    this.token = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  accessDenied() {
    localStorage.removeItem('currentUser');localStorage.removeItem('event');;
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    return false;
  }

}
