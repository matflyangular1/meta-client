import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {AuthService, SharedataService} from '../../services/index';
import {Router, NavigationEnd, NavigationStart} from '@angular/router';

export let browserRefresh = false;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public balance: string;
  public pl: string;


  constructor(private service: AuthService, private shareData: SharedataService, private router: Router) {

    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        browserRefresh = !router.navigated;
      }
      if (event instanceof NavigationEnd) {
        this.getBalance();
      }
    });
  }

  ngOnInit() {
  console.log(browserRefresh);
    // this.getBalance();
    this.shareData.getCall().subscribe(res => {
      if (res) {
        this.getBalance();
      }
    });
  }

  getBalance(): void {
    this.service.headerCall().subscribe((res) => {
      if (res.data) {
        this.balance = res.data.balance.balance;
        this.pl = res.data.balance.pl_balance;
        this.shareData.setOption('balance', res.data.balance.balance);
        this.shareData.setOption('announcement', res.data.globalCommentary);
        this.shareData.setGlobleCommantryChange(res.data.globalCommentary);
      }
    });
  }

  logout(): void {
    this.service.logout().subscribe((res) => {
      localStorage.removeItem('currentUser');localStorage.removeItem('event');;
      sessionStorage.clear();
      this.router.navigate(['/auth']);
    });
  }

}
