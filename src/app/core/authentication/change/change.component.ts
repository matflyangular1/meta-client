import {Component, OnInit, OnDestroy, Renderer2} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.css']
})
export class ChangeComponent implements OnInit, OnDestroy {
  form: FormGroup;
  errorMsg: string;
  successMsg: string;
  cPasswordType: string = 'password';
  passwordType: string = 'password';

  constructor(
    private renderer: Renderer2,
    public _router: Router,
    private formBuilder: FormBuilder,
    private _auth: AuthService
  ) {
    this.renderer.addClass(document.body, 'forget-bg');
    this.createForm();

    /*******Back button disable***********/
    window.location.hash = 'changepassword';
    window.location.hash = 'Again-No-back-button'; // again because google chrome don't insert first hash into history
    window.onhashchange = function () {
      window.location.hash = 'changepassword';
    };
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'forget-bg');
    // const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    // body.classList.remove('forget-bg');
  }

  createForm() {
    this.form = this.formBuilder.group({
      oldpassword: ['', [Validators.required]],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      cpassword: ['', [Validators.required]]
    });
  }

  get f() { return this.form.controls; }

  get frmOldpassword() {
    return this.form.get('oldpassword');
  }

  get frmPassword() {
    return this.form.get('password');
  }

  get frmCpassword() {
    return this.form.get('cpassword');
  }
  submit() {
    this.errorMsg = '';
    this.successMsg = '';
    if (this.form.valid === true) {
      this._auth.changePassword(this.form.value).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.form.reset();
      localStorage.removeItem('currentUser');
      localStorage.removeItem('event');
      localStorage.removeItem('currentUser');localStorage.removeItem('event');;
      sessionStorage.clear();
      document.cookie.split(';').forEach(function (c) {
        document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
      });
      this._router.navigate(['/auth/']);
    } else {
      this.errorMsg = res.message;
    }
  }

  viewcPassword(type) {
    if (type === 'password') {
      this.cPasswordType = 'text';
    }
    if (type === 'text') {
      this.cPasswordType = 'password';
    }
  }

  viewPassword(type) {
    if (type === 'password') {
      this.passwordType = 'text';
    }
    if (type === 'text') {
      this.passwordType = 'password';
    }
  }

  addBodyClass() {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    body.classList.add('forget-bg');
  }

}
