import { Component, OnInit, OnDestroy, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService, SharedataService } from '../../services/index';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('captchaElem') captchaElem: ElementRef;
  public logo: string;
  public form: FormGroup;
  public errorMsg: string;
  public systemInActiveMsg: string;
  public passwordType: string = 'password';
  public firstScreen: boolean = false;
  public isSubmit: boolean = false;
  public downloadUrl: any = window.location.origin !== undefined ? window.location.origin : window.location.protocol + '://' + window.location.hostname;
  public isActiveSYS: number = 0;
  public apkdownloadLink: string = '';
  public downloadTag: string;

  public siteKey: any = 'sddsdsds3434343sds';
  public reCaptchVal: number = 0;

  constructor(
    private renderer: Renderer2,
    public _router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private share: SharedataService
  ) {
    this.createForm();
    /*******Back button disable***********/
    window.location.hash = 'login';
    window.location.hash = 'Again-No-back-button'; // again because google chrome don't insert first hash into history
    window.onhashchange = function () {
      window.location.hash = 'login';
    };
    this.renderer.addClass(document.body, 'login-bg');

    this.downloadTag = 'MetaGoldTrader-2';
    // this.downloadUrl = 'http://metagoldtrader.com/APK/MetaGoldTrader-2.apk';
    this.downloadUrl += '/APK/MetaGoldTrader-2.apk';
  }

  randomNumber() {
    this.reCaptchVal = Math.floor(Math.random() * 90000) + 10000;

  }
  ngOnInit() {
    this.randomNumber()
    // this.addRecaptchaScript();
    const xhm1 = setTimeout(() => {
      clearTimeout(xhm1);
      this.isActiveSYS = Number(this.share.getSingleSettingOption('IS_MAINTENANCE'));
      this.systemInActiveMsg = this.share.getSingleSettingOption('MAINTENANCE_SETTING');
      document.getElementById('particles-js').removeAttribute('style');
    }, 500);
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'login-bg');
  }




  createForm() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      platform: ['web'],
      systemId: ['1'],
      recaptcha: ['', Validators.required]
    });
  }

  submitLogin() {
    this.errorMsg = '';
    this.isSubmit = true;
    const data = this.form.value;
    if (this.form.valid && !this.isActiveSYS && (this.reCaptchVal == data.recaptcha)) {
      this.authService.login(data).subscribe(
        res => {
          if (res.code === 444 && res.status === 0) {
            this.errorMsg = res.message;
          } else {
            // this.alertService.presentToast('Logged In');
            if (res.data.is_password_updated === 1) {
              this._router.navigate(['/auth/teams']);
            } else {
              this._router.navigate(['/auth/change']);
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    } else if (this.reCaptchVal) {
      this.errorMsg = 'Please Enter reCaptcha.. Try again..';
    }
    // this._auth.login(this.form.value).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status === 1) {
      if (res.data != null && res.code === 200) {
        if (window.localStorage !== undefined) {
          window.localStorage.setItem('currentUser', JSON.stringify(res.data));
          window.localStorage.setItem('isLoggin', '1');
        } else {
          alert('Your browser is outdated!');
        }

        if (res.data.is_password_updated.toString() === '1') {
          this._router.navigate(['/auth/teams-condtion']);
        } else {
          this._router.navigate(['/auth/change']);
        }
      }
    } else {
      this._router.navigate(['/auth/']);
      this.errorMsg = res.message;
    }
    this.isSubmit = false;
  }

  get frmuUsername() {
    return this.form.get('username');
  }

  get frmPassword() {
    return this.form.get('password');
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('event');
    // this._router.navigate(['/auth/']);
  }

  closeMsg() {
    this.errorMsg = '';
  }

  viewPassword(type) {
    if (type === 'password') {
      this.passwordType = 'text';
    }
    if (type === 'text') {
      this.passwordType = 'password';
    }
  }

  addBodyClass() {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    body.classList.add('login-bg');
  }

}
