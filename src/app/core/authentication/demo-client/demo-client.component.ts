import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services';
import swal from 'sweetalert2';
@Component({
  selector: 'app-demo-client',
  templateUrl: './demo-client.component.html',
  styleUrls: ['./demo-client.component.css']
})
export class DemoClientComponent implements OnInit {
  form: FormGroup;
  errorMsg: string;
  successMsg: string;
  cPasswordType: string = 'password';
  passwordType: string = 'password';

  constructor(
    private renderer: Renderer2,
    public _router: Router,
    private formBuilder: FormBuilder,
    private _auth: AuthService
  ) {
    this.renderer.addClass(document.body, 'forget-bg');
    this.createForm();

    /*******Back button disable***********/
    // window.location.hash = 'DemoAccount';
    window.onhashchange = function () {
      window.location.hash = 'DemoAccount';
    };
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'forget-bg');
  }

  createForm() {
    this.form = this.formBuilder.group({
      name: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)]
      ],
      username: ['', [ Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)] ],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      // balance: ['150000'],
      remark: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
    });
  }
  

  get frmName() { return this.form.get('name'); }
  get frmUsername() { return this.form.get('username'); }
  get frmPassword() { return this.form.get('password'); }
  get frmBalance() { return this.form.get('balance'); }
  get frmRemark() { return this.form.get('remark'); }

  
  submit() {
    this.errorMsg = '';
    this.successMsg = '';
    if (this.form.valid === true) {
      this._auth.demoAccount(this.form.value).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      swal.fire(
        'Success!',
        res.success.message,
        'success'
      );
      this.form.reset();
      this._router.navigate(['/auth/']);
    } else {
      swal.fire(
        'Error!',
        res.error.message,
        'error'
      );
    }
  }

  viewPassword(type) {
    if (type === 'password') {
      this.passwordType = 'text';
    }
    if (type === 'text') {
      this.passwordType = 'password';
    }
  }

  addBodyClass() {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    body.classList.add('forget-bg');
  }

}
