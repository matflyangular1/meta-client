import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ChangeComponent} from './change/change.component';
import {TeamsComponent} from './teams/teams.component';
import { DemoClientComponent } from './demo-client/demo-client.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'change',
    component: ChangeComponent
  },
  {
    path: 'demo',
    component: DemoClientComponent
  },
  {
    path: 'teams',
    component: TeamsComponent
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    ChangeComponent,
    TeamsComponent,
    DemoClientComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AuthenticationModule { }
