import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import { AuthGuard } from './share/guard/auth.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './core/authentication/authentication.module#AuthenticationModule'
  },
  {
    path: '',
    loadChildren: './modules/modules.module#ModulesModule',
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
