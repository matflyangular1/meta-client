import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BasicAuthInterceptor, ErrorInterceptor} from './share/interceptor/index';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AuthService, SharedataService, ConstantsService, ExcelService, ScriptLoaderService} from './core/services/index';
import { AuthGuard } from './share/guard/auth.guard';

import { SortbyPipe } from './share/pipes/sortby.pipe';
// import { RecaptchaDirective } from './share/directives/recaptcha.directive';

/** COMMON***************************************** */
import {BaseComponent} from './share/components/common.component';

@NgModule({
  declarations: [ AppComponent, BaseComponent, SortbyPipe],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    SharedataService,
    ConstantsService,
    ScriptLoaderService,
    // ExcelService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
