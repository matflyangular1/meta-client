export const environment = {
  production: true,
  version: '1.1.2',
  API_URL: 'https://api-client.metagoldtrader.com/api/',
  API_LIVE_URL: 'https://metagoldtrader.com:8443/dev/',
};
