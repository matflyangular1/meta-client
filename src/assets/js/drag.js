setTimeout(function () {
    
    var dragSrcEl;
    var startEvn = -1;

    function dragStart(e) {
        this.style.opacity = '0.4';
        dragSrcEl = this;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/html', this.innerHTML);
        startEvn = 1;
    };

    function dragEnter(e) {
        this.classList.add('over');
    }

    function dragLeave(e) {
        e.stopPropagation();
        this.classList.remove('over');
    }

    function dragOver(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = 'move';
        return false;
    }

    function dragDrop(e) {
        if (dragSrcEl != this) {
            dragSrcEl.innerHTML = this.innerHTML;
            this.innerHTML = e.dataTransfer.getData('text/html');
        }
        return false;
    }

    function dragEnd(e) {
        var listItens = document.querySelectorAll('.dragable-inner');
        [].forEach.call(listItens, function (item) {
            item.classList.remove('over');
        });
        this.style.opacity = '1';
        // setTimeout(function() { 
        getLastDragableItem();
        // }, 1000);
    }

    function getLastDragableItem() {
        var newIndex = [];
        var username = JSON.parse(localStorage.getItem('currentUser')).username;

        var listItens = document.querySelectorAll('.draggable-item');
        [].forEach.call(listItens, function (item, index) {
            var arr = {'new_index': index, 'market_name': item.dataset.uindex , 'username': username};
            newIndex.push(arr);
        });
        saveSoertOrder({'market': newIndex});
        // saveSoertOrder(newIndex);
    }

    function saveSoertOrder(data) {
        var token = JSON.parse(localStorage.getItem('currentUser')).token;
        var baseUrl = 'http://3.12.129.220/MetagoldClient/public/api/market-arrangement';

        if(location.origin === 'https://metagoldtrader.com'){
            baseUrl  = 'https://api-client.metagoldtrader.com/api/market-arrangement'
        }

        if(token) {
            var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var dataArr = JSON.parse(xmlhttp.responseText);
                }
            }
            xmlhttp.open('POST', baseUrl , true);
            xmlhttp.setRequestHeader("Authorization", 'Bearer ' + token);
            xmlhttp.setRequestHeader("Accept" , "application/json");
            xmlhttp.setRequestHeader("Content-Type" , "application/json");
            xmlhttp.setRequestHeader("encryption" , "false");
            
            xmlhttp.send(JSON.stringify(data));
        }
    }

    function addEventsDragAndDrop(el) {
        el.addEventListener('dragstart', dragStart, false);
        el.addEventListener('dragenter', dragEnter, false);
        el.addEventListener('dragover', dragOver, false);
        el.addEventListener('dragleave', dragLeave, false);
        el.addEventListener('drop', dragDrop, false);
        el.addEventListener('dragend', dragEnd, false);
    }

    var listItens = document.querySelectorAll('.dragable-inner');
    [].forEach.call(listItens, function (item) {
        addEventsDragAndDrop(item);
    });

}, 2000);